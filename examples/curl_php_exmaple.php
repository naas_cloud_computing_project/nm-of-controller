<?php
  $request_url = "http://128.110.152.105:8090/"; // URL to send request to by default
  
  if(isset($_POST['request']) && isset($_POST['request']['submit'])) { // Check if sending request
    $request = $_POST['request']; // Array with request info
    
    $ch = curl_init(); // Start cURL
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, $request_url . $request['action']); // Give cURL destination
    
    if($request['username'] != "" && $request['password'] != "") { // Include username and password in request
      curl_setopt($ch, CURLOPT_USERPWD, $request['username'] . ":" . $request['password']);
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $curl_data = curl_exec($ch);
    curl_close($ch);
    
    $json_response = json_decode($curl_data, true); // Turn json into associative array
    
    $response_str = '<h2>Response:</h2><pre>';
    if(isset($json_response['error'])) // If error
      $response_str .= 'Error: ' . $json_response['error'];
    else // Else display output
      $response_str .= $json_response['output'];
    $response_str .= '</pre><hr />';
  }
?>
<html>
  <head>
    <title>Michael Rowden - Cloud Computing - Homework 3</title>
  </head>
  <body>
    <h1>Michael Rowden - Cloud Computing - Homework 3</h1>
    <hr />
    <?php echo isset($response_str) ? $response_str : ''; ?>
    <form method="post">
      <h3>Fill in the form to send your request</h3>
      <label>Username</label>
      <input type="text" name="request[username]" />
      <br />
      <label>Password</label>
      <input type="password" name="request[password]" />
      <br />
      <label><?php echo $request_url; ?></label>
      <input type="text" name="request[action]" />
      <br />
      <input type="submit" value="Send" name="request[submit]" />
    </form>
  </body>
</html>
