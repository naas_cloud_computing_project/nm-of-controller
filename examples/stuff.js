{ 
 "nodes" = [
    {id: 0, type: 'host', ip: '128.0.0.1', color: red},
    {id: 1, type: 'switch', ip: '128.0.0.2', color: blue},
    {id: 2, type: 'switch', ip: '128.0.0.3', color: blue},
    {id: 3, type: 'switch', ip: '128.0.0.4', color: blue},
    {id: 4, type: 'switch', ip: '128.0.0.5', color: blue},
    {id: 5, type: 'host', ip: '128.0.0.6', color: red},
    {id: 6, type: 'switch', ip: '128.0.0.7', color: blue}
  ],
  "links" = [
    {id: 0, source: nodes[0], target: nodes[1], left: false, right: false },
    {id: 1, source: nodes[0], target: nodes[2], left: false, right: false },
    {id: 2, source: nodes[1], target: nodes[3], left: false, right: false },
    {id: 3, source: nodes[2], target: nodes[4], left: false, right: false },
    {id: 4, source: nodes[3], target: nodes[5], left: false, right: false },
    {id: 5, source: nodes[4], target: nodes[6], left: false, right: false },
    {id: 6, source: nodes[6], target: nodes[5], left: false, right: false },
    {id: 7, source: nodes[1], target: nodes[2], left: false, right: false },
    {id: 8, source: nodes[3], target: nodes[4], left: false, right: false }
  ]
}