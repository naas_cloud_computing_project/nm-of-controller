package net.floodlightcontroller.nm.xml;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import net.floodlightcontroller.nm.openflow.customservices.IRouteFinderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by chemo_000 on 9/18/2015.
 */
public class ConfigParser
{
    protected static Logger log = LoggerFactory.getLogger(ConfigParser.class);

    public int BW_TO_QUEUE_ID_RATIO = 20;

    public double MAX_HOST_LINK_BW = 100.0;

    public double MAX_SWITCH_LINK_BW = 200.0;

    public int FLOW_IDLE_TIMEOUT = 0;

    public int FLOW_HARD_TIMEOUT = 0;

    public IRouteFinderService.Type ROUTING_TYPE = IRouteFinderService.Type.NM;

    public boolean EXPORT_RESULTS = false;

    public boolean DYNAMIC_GRAPH = false;

    private static ConfigParser instance = new ConfigParser("nm-config.xml");

    public static ConfigParser getInstance()
    {
        return instance;
    }

    private ConfigParser(String url)
    {
        File fXmlFile = new File(url);

        try
        {
            parseFile(new FileInputStream(fXmlFile));
        } catch (Exception ex)
        {
            log.error("[NM-XMLParser]During parsing xml file " + url + " following exception occurred: " + ex.getMessage(), ex);
        }

        if (log.isInfoEnabled())
            log.info("[NM-XMLParser] XML Parser successfully read next values:"
                    + "\nBW_TO_QUEUE_ID_RATIO = " + BW_TO_QUEUE_ID_RATIO
                    + "\nMAX_HOST_LINK_BW = " + MAX_HOST_LINK_BW
                    + "\nMAX_SWITCH_LINK_BW = " + MAX_SWITCH_LINK_BW
                    + "\nFLOW_IDLE_TIMEOUT = " + FLOW_IDLE_TIMEOUT
                    + "\nFLOW_HARD_TIMEOUT = " + FLOW_HARD_TIMEOUT
                    + "\nROUTING_TYPE = " + ROUTING_TYPE
                    + "\nEXPORT_RESULTS = " + EXPORT_RESULTS
                    + "\nDYNAMIC_GRAPH = " + DYNAMIC_GRAPH);
    }

    protected ConfigParser()
    {
    }

    protected void parseFile(InputStream fXmlStream) throws ParserConfigurationException, IOException, SAXException
    {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlStream);
        doc.getDocumentElement().normalize();
        Element root = doc.getDocumentElement();

        try
        {
            BW_TO_QUEUE_ID_RATIO = Integer.valueOf(root.getElementsByTagName("queue_id_bw_ratio").item(0).getAttributes().getNamedItem("val").getTextContent());
        } catch (Exception ex)
        {
            log.error("[NM-XMLParser] Exception occurred during parsing BW_TO_QUEUE_ID_RATIO value, default value will be used:" + ex.getMessage(), ex);
        }
        try
        {
            MAX_HOST_LINK_BW = Double.valueOf(root.getElementsByTagName("max_host_to_switch_bw").item(0).getAttributes().getNamedItem("val").getTextContent());
        } catch (Exception ex)
        {
            log.error("[NM-XMLParser] Exception occurred during parsing MAX_HOST_LINK_BW value, default value will be used:" + ex.getMessage(), ex);
        }
        try
        {
            MAX_SWITCH_LINK_BW = Double.valueOf(root.getElementsByTagName("max_switch_to_switch_bw").item(0).getAttributes().getNamedItem("val").getTextContent());
        } catch (Exception ex)
        {
            log.error("[NM-XMLParser] Exception occurred during parsing MAX_SWITCH_LINK_BW value, default value will be used:" + ex.getMessage(), ex);
        }
        try
        {
            FLOW_IDLE_TIMEOUT = Integer.valueOf(root.getElementsByTagName("flow_idle_timeout").item(0).getAttributes().getNamedItem("val").getTextContent());
        } catch (Exception ex)
        {
            log.error("[NM-XMLParser] Exception occurred during parsing FLOW_IDLE_TIMEOUT value, default value will be used:" + ex.getMessage(), ex);
        }
        try
        {
            FLOW_HARD_TIMEOUT = Integer.valueOf(root.getElementsByTagName("flow_hard_timeout").item(0).getAttributes().getNamedItem("val").getTextContent());
        } catch (Exception ex)
        {
            log.error("[NM-XMLParser] Exception occurred during parsing FLOW_HARD_TIMEOUT value, default value will be used:" + ex.getMessage(), ex);
        }
        try
        {
            ROUTING_TYPE = IRouteFinderService.Type.valueOf(root.getElementsByTagName("routing_type").item(0).getAttributes().getNamedItem("val").getTextContent().toUpperCase());
        } catch (Exception ex)
        {
            log.error("[NM-XMLParser] Exception occurred during parsing ROUTING_TYPE value, default value will be used:" + ex.getMessage(), ex);
        }
        try
        {
            EXPORT_RESULTS = Boolean.valueOf(root.getElementsByTagName("export_results").item(0).getAttributes().getNamedItem("val").getTextContent());
        } catch (Exception ex)
        {
            log.error("[NM-XMLParser] Exception occurred during parsing EXPORT_RESULTS value, default value will be used:" + ex.getMessage(), ex);
        }
        try
        {
            DYNAMIC_GRAPH = Boolean.valueOf(root.getElementsByTagName("dynamic_graph").item(0).getAttributes().getNamedItem("val").getTextContent());
        } catch (Exception ex)
        {
            log.error("[NM-XMLParser] Exception occurred during parsing EXPORT_RESULTS value, default value will be used:" + ex.getMessage(), ex);
        }
    }
}
