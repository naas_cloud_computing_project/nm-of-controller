package net.floodlightcontroller.nm.mapping.routing;

import net.floodlightcontroller.nm.allocation.RouteBroker;
import net.floodlightcontroller.nm.discovery.model.INode;
import net.floodlightcontroller.nm.utils.CartesianUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by chemo_000 on 4/2/2015.
 */
public class SplittableNM
{
    private static Logger log = LoggerFactory.getLogger(SplittableNM.class);

    private CartesianUtils cu;
    private RouteFinder routeFinder;

    public SplittableNM(CartesianUtils cartesianUtils, RouteFinder routeFinder)
    {
        this.cu = cartesianUtils;
        this.routeFinder = routeFinder;
    }

    protected <T> Map<List<INode<T>>, Double> findSplittableRoutes(INode<T> src, INode<T> dst, double bw)
    {
        //Build Neighborhoods
        List<Set<INode<T>>> neighborhoods = new LinkedList<>();
        neighborhoods.add(Collections.singleton(src));//add root ring which contains only src node
        neighborhoods = routeFinder.getNb().buildNextNeighborhoods(dst, neighborhoods, 0);


        //Perform backward pass and estimate bw
        return findRoutesAndBwWithLimitInt(src, dst, bw, neighborhoods, new HashMap<List<INode<T>>, Double>(), 0);
    }

    //todo refactor
    private <T> Map<List<INode<T>>, Double> findRoutesAndBwWithLimitInt(INode<T> src, INode<T> dst, double bw,
                                                                        List<Set<INode<T>>> neighborhoods,
                                                                        Map<List<INode<T>>, Double> routesAndBw, int iter)
    {
        //preserve last neighborhood for next iterations
        Set<INode<T>> lastNeighborhood = neighborhoods.set(neighborhoods.size() - 1, Collections.singleton(dst));

        Set<List<INode<T>>> routes = cu.cartesianProduct(neighborhoods);

        if (log.isTraceEnabled())
            log.trace("[NM-Routing]Next routes were found for " + src + "->" + dst + " iter[" + iter + "]:" + routes);

        //todo wrapper for bw type
        double currentBw = routeFinder.getRe().allocateSuitableRoutes(routes, routesAndBw, bw, true);

        iter++;// increment iteration number by 1

        if (log.isDebugEnabled())
            log.debug("[NM-Routing]Next suitable routes with req bw were found for " + src + "->" + dst
                    + " iter[" + iter + "]:" + routesAndBw);

        if (currentBw >= bw)//todo optional rebuild condition as in paper R total <= R specified
            return routesAndBw;
        else if (iter >= 16)//todo optional integrate with Dijkstra currently ttl of RIP is used
        {
            for (Map.Entry<List<INode<T>>, Double> route : routesAndBw.entrySet())//move to RouteEstimator
                RouteBroker.getInstance().releaseSpecifiedResources(route.getKey(), route.getValue());

            String errMsg = "VL request: src=" + src.getID() + " dst=" + dst.getID() + " bw=" + bw
                    + " is_splittable=" + true + " can't be satisfied by NM!";
            log.error(errMsg);

            throw new RouteFinderException(errMsg);
        } else
        {
//            if we didn't fill bw criteria we add one more neighborhood and repeat all steps again
            neighborhoods.set(neighborhoods.size() - 1, lastNeighborhood); //return preserved last neighborhood back
            routeFinder.getNb().addNextNeighborhood(neighborhoods);

            return findRoutesAndBwWithLimitInt(src, dst, bw - currentBw, neighborhoods, routesAndBw, iter);
        }
    }
}
