package net.floodlightcontroller.nm.mapping.routing;

import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by chemo_000 on 5/24/2016.
 */
public class GeneralNM
{
    protected static Logger log = LoggerFactory.getLogger(GeneralNM.class);

    private RouteFinder rf;

    public GeneralNM(RouteFinder rf)
    {
        this.rf = rf;
    }

    public <T> Map<List<INode<T>>, Double> findBwDelayPath(INode<T> src, INode<T> dst, double bw, double cost)
    {
        if (cost > 0)
            return computeBwDelayPath(src, dst, bw, cost);
        else
            throw new RuntimeException("General NM currently does not support only BW constraint case," +
                    " please specify cost constraint!");
    }

    private <T> Map<List<INode<T>>, Double> computeBwDelayPath(INode<T> src, INode<T> dst, double bw, double cost)
    {
        //prune all links
        rf.getTopo().prepareTopoForDijkstra(bw, false);

        //forward pass - build neighborhoods with look back concept been applied
        List<Map<INode<T>, Double>> neighborhoods = buildNeighborhoods(src, dst, bw, cost);

        //backward pass with look back concept and dominant paths search space reduction techniques
        return findOptimalPathWithConstraintsInt(src, dst, bw, cost, neighborhoods, 0, 1);
    }

    private <T> Map<List<INode<T>>, Double> findOptimalPathWithConstraintsInt(INode<T> src, INode<T> dst, double bw, double cost,
                                                                              List<Map<INode<T>, Double>> neighborhoods,
                                                                              int pathsNum, int iter)
    {
        Map<List<INode<T>>, Double> pathCandidates = findPathCandidates(src, dst, cost, neighborhoods);

        try
        {
            return getPathToInt(pathCandidates, bw, cost);
        } catch (Exception e)
        {
            if (log.isDebugEnabled())
                log.debug("[NM-RouteFinder: GenNM] At iteration=" + iter + " General NM  didn't found any solution"
                        + " among path candidates set of size=" + pathCandidates.size() + "! Total paths=" + pathsNum);
        }

        buildNextNeighborhoods(neighborhoods, src, dst, bw, cost);

        return findOptimalPathWithConstraintsInt(src, dst, bw, cost, neighborhoods, pathsNum, ++iter);
    }

    private <T> List<Map<INode<T>, Double>> buildNeighborhoods(INode<T> src, INode<T> dst, double bw, double cost)
    {
        List<Map<INode<T>, Double>> neighborhoods = new ArrayList<>();

        Map<INode<T>, Double> currentNeighborhood = new LinkedHashMap<>();
        currentNeighborhood.put(src, 0.0); //put 0 initial cost

        neighborhoods.add(currentNeighborhood);

        while (!currentNeighborhood.containsKey(dst))
        {
            Map<INode<T>, Double> newNeighborhood = new LinkedHashMap<>();

            for (Map.Entry<INode<T>, Double> entry : currentNeighborhood.entrySet())
            {
                INode<T> n = entry.getKey();

                for (Map.Entry<INode<T>, ILink> neighbor : n.getNeighbors().entrySet())
                {
                    double newCostDist = entry.getValue() + neighbor.getValue().getAvCost();

                    //@todo: do not look ahead during forward pass as it's expensive on large scale
                    if (newCostDist <= cost)//+ neighbor.getKey().dstAheadVector[1] <= cost)
                    {
                        if (newNeighborhood.containsKey(neighbor.getKey()) &&
                                newNeighborhood.get(neighbor.getKey()) > newCostDist)
                        {
                            newNeighborhood.remove(neighbor.getKey());
                            newNeighborhood.put(neighbor.getKey(), newCostDist);
                        } else
                            newNeighborhood.put(neighbor.getKey(), newCostDist);
                    }
                }
            }

            if (newNeighborhood.isEmpty() || neighborhoods.size() >= rf.getTopo().getKnownNodes().size())
                throw new RuntimeException("NM General can't provide path from " + src.getID() + " to " + dst.getID()
                        + " because of constraints: bw=" + bw + " and cost=" + cost
                        + ", or the destination is unreachable!!!");

            neighborhoods.add(newNeighborhood);
            currentNeighborhood = newNeighborhood;
        }

        return neighborhoods;
    }

    private <T> List<Map<INode<T>, Double>> buildNextNeighborhoods(List<Map<INode<T>, Double>> neighborhoods,
                                                                   INode<T> src, INode<T> dst, double bw, double cost)
    {
        Map<INode<T>, Double> currentNeighborhood = neighborhoods.get(neighborhoods.size() - 1);
        currentNeighborhood.remove(dst);
        while (!currentNeighborhood.containsKey(dst))
        {
            Map<INode<T>, Double> newNeighborhood = new LinkedHashMap<>();
            for (Map.Entry<INode<T>, Double> entry : currentNeighborhood.entrySet())
            {
                INode<T> n = entry.getKey();
                for (Map.Entry<INode<T>, ILink> neighbor : n.getNeighbors().entrySet())
                {
                    double newCostDist = entry.getValue() + neighbor.getValue().getAvCost();
                    //@todo: do not look ahead during forward pass as it's expensive on large scale
                    if (newCostDist <= cost)//+ neighbor.getKey().dstAheadVector[1] <= cost)
                    {
                        if (newNeighborhood.containsKey(neighbor.getKey()) &&
                                newNeighborhood.get(neighbor.getKey()) > newCostDist)
                        {
                            newNeighborhood.remove(neighbor.getKey());
                            newNeighborhood.put(neighbor.getKey(), newCostDist);
                        } else
                            newNeighborhood.put(neighbor.getKey(), newCostDist);
                    }
                }
            }
            if (newNeighborhood.isEmpty() || neighborhoods.size() >= rf.getTopo().getKnownNodes().size())
                throw new RuntimeException("NM can't provide path from " + src.getID() + " to " + dst.getID()
                        + " because of constraints: bw=" + bw + " and cost=" + cost
                        + ", or the destination is unreachable!!!");
            neighborhoods.add(newNeighborhood);
            currentNeighborhood = newNeighborhood;
        }
        return neighborhoods;
    }

    private <T> Map<List<INode<T>>, Double> findPathCandidates(INode<T> src, INode<T> dst, double cost,
                                                               List<Map<INode<T>, Double>> neighborhoods)
    {
        Map<List<INode<T>>, Double> paths = new LinkedHashMap<>();
        paths.put(Collections.singletonList(dst), 0.0);

        dst.setDominantPaths(paths);

        int k = 2;

        Map<INode<T>, Double> prevNeighborhood = neighborhoods.get(neighborhoods.size() - k);

        while (k <= neighborhoods.size())
        {
            if (paths.isEmpty())
                return Collections.emptyMap();
            else
            {
                Map<List<INode<T>>, Double> tempPaths = new LinkedHashMap<>();
                for (final Map.Entry<List<INode<T>>, Double> path_entry : paths.entrySet())
                {
                    INode<T> u = path_entry.getKey().get(0);

                    for (final Map.Entry<INode<T>, ILink> v : u.getNeighbors().entrySet())
                    {
                        //check if in intersection [NM main feature]
                        if (prevNeighborhood.containsKey(v.getKey()))
                        {
                            final double newCostDist = path_entry.getValue() + v.getValue().getAvCost();
                            //look back during backward pass
                            double learnedCostDist = prevNeighborhood.get(v.getKey());
                            if (newCostDist + learnedCostDist <= cost)
                            {
                                //create new path candidate
                                List<INode<T>> newPath = new ArrayList<INode<T>>()
                                {
                                    {
                                        add(v.getKey());
                                        addAll(path_entry.getKey());
                                    }
                                };
                                //if new path is not dominated by existing paths to node or to source keep it
                                if (!v.getKey().isDominatedByNodePaths(newPath, newCostDist) &&
                                        !src.isDominatedByNodePaths(newPath, newCostDist))
                                {
                                    Set<List<INode<T>>> removeOldDominantPaths = v.getKey()
                                            .addDominantPath(newPath, newCostDist);
                                    for (List<INode<T>> pathToRemove : removeOldDominantPaths)
                                        tempPaths.remove(pathToRemove);
                                    tempPaths.put(newPath, newCostDist);
                                }
                            }
                        }
                    }
                }
                // set up for next hop cycle
                paths = tempPaths;
                ++k;
                if (k <= neighborhoods.size())
                    prevNeighborhood = neighborhoods.get(neighborhoods.size() - k);
            }
        }
        return paths;
    }

    private <T> Map<List<INode<T>>, Double> getPathToInt(Map<List<INode<T>>, Double> pathCandidates,
                                                         double bw, double cost)
    {
        for (Map.Entry<List<INode<T>>, Double> entry_path : pathCandidates.entrySet())
        {
            if (entry_path.getValue() <= cost)
            {
                List<INode<T>> path = entry_path.getKey();

                Map<List<INode<T>>, Double> map = new HashMap<>();
                map.put(path, bw);

                return map;
            }
        }
        throw new RuntimeException("General NM didn't found path during constraints validation step!");
    }
}