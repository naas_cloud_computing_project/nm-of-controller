/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.floodlightcontroller.nm.mapping.routing;


import net.floodlightcontroller.nm.discovery.model.INode;
import net.floodlightcontroller.nm.discovery.model.ITopology;
import net.floodlightcontroller.nm.openflow.customservices.IRouteFinderService;
import net.floodlightcontroller.nm.utils.CartesianUtils;
import net.floodlightcontroller.nm.utils.LogWriter;
import net.floodlightcontroller.nm.utils.UsedSwitchesHolder;
import net.floodlightcontroller.nm.xml.ConfigParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * @author D-MAN
 */
public class RouteFinder implements IRouteFinderService
{
    private static Logger log = LoggerFactory.getLogger(RouteFinder.class);
    protected CartesianUtils cu;
    private NonSplittableNMNew nonSpNM;
    private GeneralNM genNM;
    private SplittableNM spNM;
    protected Dijkstra ed;
    private IBF ibf;
    private EBFS ebfs;
    private RouteEstimator re;
    private NeighborhoodBuilder nb;
    private ITopology t;
    private Type type;

    public RouteFinder(ITopology t)
    {
        this.t = t;
        this.cu = new CartesianUtils();
        this.nonSpNM = new NonSplittableNMNew(this);
        this.genNM = new GeneralNM(this);
        this.spNM = new SplittableNM(cu, this);
        this.ed = new Dijkstra(this);
        this.ibf = new IBF(this);
        this.ebfs = new EBFS(this);
        this.re = new RouteEstimator();
        this.nb = new NeighborhoodBuilder();
        this.type = ConfigParser.getInstance().ROUTING_TYPE;
    }

    /**
     * Internal constructor for tests
     *
     * @param t
     * @param cu
     * @param re
     * @param nonSpNM
     * @param type
     */
    protected RouteFinder(ITopology t, CartesianUtils cu, RouteEstimator re, NonSplittableNMNew nonSpNM, Type type)
    {
        this.t = t;
        this.cu = cu;
        this.nonSpNM = new NonSplittableNMNew(this);
        this.spNM = new SplittableNM(cu, this);
        this.re = re;
        this.type = type;
        this.nonSpNM = nonSpNM;
    }

    public ITopology getTopo()
    {
        return this.t;
    }

    protected RouteEstimator getRe()
    {
        return this.re;
    }

    protected NeighborhoodBuilder getNb()
    {
        return this.nb;
    }

    /**
     * General NM approach algorithm
     *
     * @param srcID        - id of source node
     * @param dstID        - id of destination node
     * @param isSplittable - does virtual link contain several physical path
     * @param bw           - requested bw
     * @return
     */
    public synchronized <T> Map<List<INode<T>>, Double> findRoutesAndBw(T srcID, T dstID, boolean isSplittable, double bw)
    {
        return findRoutesAndBwCost(srcID, dstID, isSplittable, bw, 0);
    }

    /**
     * General NM approach algorithm
     *
     * @param srcID        - id of source node
     * @param dstID        - id of destination node
     * @param isSplittable - does virtual link contain several physical path
     * @param bw           - requested bw
     * @return
     */
    public synchronized <T> Map<List<INode<T>>, Double> findRoutesAndBwCost(T srcID, T dstID, boolean isSplittable, double bw, double cost)
    {
        INode<T> src = t.getNodeWithID(srcID);
        INode<T> dst = t.getNodeWithID(dstID);

        Map<List<INode<T>>, Double> result;

        if (src == null || dst == null)
            return Collections.EMPTY_MAP;
        else
        {
            long startPathComp = System.nanoTime();
            switch (type)
            {
                case NM:
                    if (isSplittable)
                        result = spNM.findSplittableRoutes(src, dst, bw);
                    else
                        result = nonSpNM.findNonSplittableRoute(src, dst, bw, cost);
                    break;
                case NMGEN:
                    result = genNM.findBwDelayPath(src, dst, bw, cost);
                    break;
                case ED:
                    result = ed.findBwDelayPath(src, dst, bw, cost);
                    break;
                case IBF:
                    result = ibf.findBwDelayPath(src, dst, bw, cost);
                    break;
                case EBFS:
                    result = ebfs.findBwDelayPath(src, dst, bw, cost);
                    break;
                default:
                    throw new RuntimeException("Unknown Routing Type!");
            }

            if (!isSplittable)
            {
                long pathCompTime = (System.nanoTime() - startPathComp) / 1000;
                //allocate found path
                Map.Entry<List<INode<T>>, Double> path = result.entrySet().iterator().next();

                if (log.isDebugEnabled())
                    log.debug("[NM-RouteFinder] Path with next number of hops = " + (path.getKey().size() - 1)
                            + " and cost=" + path.getKey().get(path.getKey().size() - 1).getMinDist() + " is found!");

                long startPathAlloc = System.nanoTime();
                re.allocateRoute(path.getKey(), path.getValue());
                long pathAllocTime = (System.nanoTime() - startPathAlloc) / 1000;

                if (ConfigParser.getInstance().EXPORT_RESULTS)
                {
                    UsedSwitchesHolder.getInstance().addUsedSwitches(path.getKey());
                    //write num of switches, bw, path length, VL path comp, VL path alloc
                    LogWriter.getInstance().logInfoToBuffer(UsedSwitchesHolder.getInstance().getNumOfUsedSwitches() + ", "
                            + path.getValue() + ", " + (path.getKey().size() - 1) + "," + pathCompTime + "," + pathAllocTime + ",");
                }
            }

            return result;
        }
    }
}
