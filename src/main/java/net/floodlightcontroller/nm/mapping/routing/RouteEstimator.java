package net.floodlightcontroller.nm.mapping.routing;

import net.floodlightcontroller.nm.allocation.RouteBroker;
import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chemo_000 on 2/6/2015.
 */
class RouteEstimator
{
    private RouteBroker rb;

    public RouteEstimator()
    {
        this.rb = RouteBroker.getInstance();
    }

    protected RouteEstimator(RouteBroker rb)
    {
        this.rb = rb;
    }

    //todo add logs
    protected <T> double allocateSuitableRoutes(Collection<List<INode<T>>> routes, Map<List<INode<T>>, Double> routesAndBw,
                                            double bw, boolean isSplittable)
    {
        if (isSplittable)
            return allocateRoutesInSplittableCase(routes, routesAndBw, bw);
        else
            return allocateRoutesInNonSplittableCase(routes, routesAndBw, bw);
    }

    protected <T> void allocateRoute(List<INode<T>> route, double bw)
    {
        rb.reserveResources(route, bw);
    }

    //todo make wrapper for bw type and add more comments
    private <T> double allocateRoutesInSplittableCase(Collection<List<INode<T>>> routes,
                                                  Map<List<INode<T>>, Double> routesAndBw, double bw)
    {
        double bwInt = 0;

        for (List<INode<T>> route : routes)
        {
            double avBw = calculateBwForRoute(route);

            if (avBw == 0)
                continue;

            if (bwInt + avBw >= bw)//todo optional rebuild condition as in paper R total <= R specified
            {
                rb.reserveResources(route, bw - bwInt);
                routesAndBw.put(route, bw - bwInt);
                bwInt = bw;
                break;

            } else
            {
                rb.reserveResources(route, avBw);
                routesAndBw.put(route, avBw);
                bwInt += avBw;
            }
        }
        return bwInt;
    }


    private <T> double allocateRoutesInNonSplittableCase(Collection<List<INode<T>>> routes,
                                                     Map<List<INode<T>>, Double> routesAndBw, double bw)
    {
        double bwInt = 0;

        for (List<INode<T>> route : routes)
        {
            double avBw = calculateBwForRoute(route);

            if (avBw >= bw)//todo optional rebuild condition as in paper R total <= R specified
            {
                rb.reserveResources(route, bw);
                routesAndBw.put(route, bw);
                bwInt = bw;
                break;
            }
        }
        return bwInt;
    }

    //todo refactor
    private <T> double calculateBwForRoute(List<INode<T>> route)
    {
        double bw = 0;
        if (!route.isEmpty())
        {
            INode<T> prevNode = route.get(0);//assign src node at the beginning
//            need to find min bandwidth among the path
            double minBW = Double.MAX_VALUE;
            for (int i = 1; i < route.size(); i++)
            {
                INode n = route.get(i);
                Map<INode<T>, ILink> neighbors = prevNode.getNeighbors();
                ILink link = neighbors.get(n);
                double bwInt = link.getBw();

                if (bwInt < minBW)
                    minBW = bwInt;

                prevNode = n;
            }
            bw = minBW;//as our condition is R total<=R specified
        }
        return bw;
    }


    private <T> Map<List<INode<T>>, Double> calculateBwForRoutes(Collection<List<INode<T>>> routes)
    {
        Map<List<INode<T>>, Double> bwAndRoutes = new HashMap<>();

        for (List<INode<T>> route : routes)
            bwAndRoutes.put(route, calculateBwForRoute(route));

        return bwAndRoutes;
    }
}
