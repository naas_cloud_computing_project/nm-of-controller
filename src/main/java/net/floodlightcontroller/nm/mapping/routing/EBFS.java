package net.floodlightcontroller.nm.mapping.routing;

import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by chemo_000 on 5/25/2016.
 */
public class EBFS
{
    protected static Logger log = LoggerFactory.getLogger(EBFS.class);
    private RouteFinder rf;

    public EBFS(RouteFinder rf)
    {
        this.rf = rf;
    }

    public <T> Map<List<INode<T>>, Double> findBwDelayPath(INode<T> src, INode<T> dst, double bw, double cost)
    {
        if (cost > 0)
            return findBwDelayPathInt(src, dst, bw, cost);
        else
            throw new RuntimeException("EBFS currently does not support only BW constraint case," +
                    " please specify cost constraint!");
    }

    private <T> Map<List<INode<T>>, Double> findBwDelayPathInt(INode<T> src, INode<T> dst, double bw, double cost)
    {
        //prune all links
        rf.getTopo().prepareTopoForDijkstra(bw, false);

        lookAhead(dst);

        //build initial paths
        Map<List<INode<T>>, Double> pathCandidates = findPathCandidates(src, dst, cost);

        //check and continue with longer paths if needed
        return findOptimalPathWithConstraintsInt(dst, bw, cost, pathCandidates, 0, 1);
    }

    private <T> Map<List<INode<T>>, Double> findOptimalPathWithConstraintsInt(INode<T> dst, double bw, double cost,
                                                                              Map<List<INode<T>>, Double> pathCandidates,
                                                                              int pathsNum, int iter)
    {
        pathsNum += pathCandidates.size();
        try
        {
            return getPathToInt(dst, pathCandidates, bw, cost);
        } catch (Exception e)
        {
            if (log.isDebugEnabled())
                log.debug("[NM-RouteFinder: EBFS] At iteration=" + iter + " EBFS  didn't found any solution"
                        + " among path candidates set of size=" + pathCandidates.size() + "! Total paths=" + pathsNum);
        }

        pathCandidates = findPathNextGenCandidates(pathCandidates, dst, cost);

        return findOptimalPathWithConstraintsInt(dst, bw, cost, pathCandidates, pathsNum, ++iter);
    }

    private <T> void lookAhead(INode<T> dst)
    {
        rf.ed.computeAllCostPaths(dst);
    }

    private <T> Map<List<INode<T>>, Double> findPathCandidates(INode<T> src, INode<T> dst, double cost)
    {
        Map<List<INode<T>>, Double> paths = new LinkedHashMap<>();
        List<INode<T>> initialPath = Collections.singletonList(src);
        paths.put(initialPath, 0.0);

        src.setDominantPaths(paths);

        int length = 1;
        boolean needIteration = true;

        while (needIteration && length < rf.getTopo().getKnownNodes().size())
        {
            Map<List<INode<T>>, Double> tempPaths = new LinkedHashMap<>();
            for (final Map.Entry<List<INode<T>>, Double> path_entry : paths.entrySet())
            {
                INode<T> u = path_entry.getKey().get(path_entry.getKey().size() - 1);
                for (final Map.Entry<INode<T>, ILink> v : u.getNeighbors().entrySet())
                {
                    double newCostDist = path_entry.getValue() + v.getValue().getAvCost();
                    //look-ahead during exhaustive search
                    double predictedCostDist = v.getKey().getMinDist();
                    //@todo we do not need to check for complex paths - they cannot be dominant unless NWC present!
                    if (newCostDist + predictedCostDist <= cost)
                    {
                        //check if dst was found
                        if (v.getKey().equals(dst))
                            needIteration = false;

                        //create new path candidate
                        List<INode<T>> newPath = new ArrayList<INode<T>>()
                        {
                            {
                                addAll(path_entry.getKey());
                                add(v.getKey());
                            }
                        };
                        //if new path is not dominated by existing paths to node or to source add it
                        if (!v.getKey().isDominatedByNodePaths(newPath, newCostDist) &&
                                !dst.isDominatedByNodePaths(newPath, newCostDist))
                        {
                            Set<List<INode<T>>> removeOldDominantPaths = v.getKey().addDominantPath(newPath, newCostDist);
                            for (List<INode<T>> pathToRemove : removeOldDominantPaths)
                                tempPaths.remove(pathToRemove);
                            tempPaths.put(newPath, newCostDist);
                        }
                    } //else if (log.isDebugEnabled())
//                        log.debug("[NM-RouteFinder: EBFS] Look Ahead: node[" + v.getKey().getID() + "], cost to it="
//                                + newCostDist + " residual cost=" + predictedCostDist);
                }
            }
            // set up for next hop cycle
            paths = tempPaths;
            ++length;
        }
        if (!needIteration && !paths.isEmpty())
            return paths;
        else
            throw new RuntimeException("EBFS can't provide path from " + src.getID() + " to " + dst.getID()
                    + " because of constraints: cost=" + cost
                    + ", or the destination is unreachable!!!");
    }

    private <T> Map<List<INode<T>>, Double> findPathNextGenCandidates(Map<List<INode<T>>, Double> paths, INode<T> dst,
                                                                      double cost)
    {
        int length = paths.keySet().iterator().next().size();
        boolean needIteration = true;

        while (needIteration && length < rf.getTopo().getKnownNodes().size())
        {
            Map<List<INode<T>>, Double> tempPaths = new LinkedHashMap<>();
            for (final Map.Entry<List<INode<T>>, Double> path_entry : paths.entrySet())
            {
                INode<T> u = path_entry.getKey().get(path_entry.getKey().size() - 1);
                for (final Map.Entry<INode<T>, ILink> v : u.getNeighbors().entrySet())
                {
                    double newCostDist = path_entry.getValue() + v.getValue().getAvCost();
                    //look-ahead during exhaustive search
                    double predictedCostDist = v.getKey().getMinDist();
                    //@todo we do not need to check for complex paths - they cannot be dominant unless NWC present!
                    if (newCostDist + predictedCostDist <= cost)
                    {
                        //check if dst was found
                        if (v.getKey().equals(dst))
                            needIteration = false;
                        //create new path candidate
                        List<INode<T>> newPath = new ArrayList<INode<T>>()
                        {
                            {
                                addAll(path_entry.getKey());
                                add(v.getKey());
                            }
                        };
                        //if new path is not dominated by existing paths to node or to source add it
                        if (!v.getKey().isDominatedByNodePaths(newPath, newCostDist) &&
                                !dst.isDominatedByNodePaths(newPath, newCostDist))
                        {
                            Set<List<INode<T>>> removeOldDominantPaths = v.getKey().addDominantPath(newPath, newCostDist);
                            for (List<INode<T>> pathToRemove : removeOldDominantPaths)
                                tempPaths.remove(pathToRemove);
                            tempPaths.put(newPath, newCostDist);
                        }
                    }
                }
            }
            // set up for next hop cycle
            paths = tempPaths;
            ++length;
        }
        if (!needIteration && !paths.isEmpty())
            return paths;
        else
            throw new RuntimeException("EBFS can't provide path in next iterations to " + dst.getID()
                    + " because of constraints: cost=" + cost
                    + ", or the destination is unreachable!!!");
    }

    private <T> Map<List<INode<T>>, Double> getPathToInt(INode<T> dst, Map<List<INode<T>>, Double> pathCandidates,
                                                         double bw, double cost)
    {
        for (Map.Entry<List<INode<T>>, Double> entry_path : pathCandidates.entrySet())
        {
            List<INode<T>> path = entry_path.getKey();
            //check if path lead to dst and satisfies constraints
            if (path.get(path.size() - 1).equals(dst) && entry_path.getValue() <= cost)
            {
                Map<List<INode<T>>, Double> map = new HashMap<>();
                map.put(path, bw);

                return map;
            }
        }
        throw new RuntimeException("EBFS didn't found path during constraints validation step!");
    }
}
