package net.floodlightcontroller.nm.mapping.routing;

import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;
import net.floodlightcontroller.nm.discovery.model.ITopology;
import net.floodlightcontroller.nm.utils.CartesianUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by chemo_000 on 4/2/2015.
 */
public class NonSplittableNMNew
{
    protected static Logger log = LoggerFactory.getLogger(NonSplittableNMNew.class);
    private RouteFinder routeFinder;

    public NonSplittableNMNew(RouteFinder routeFinder)
    {
        this.routeFinder = routeFinder;
    }

    protected <T> Map<List<INode<T>>, Double> findNonSplittableRoute(INode<T> src, INode<T> dst, double bw, double cost)
    {
        if (cost > 0)
            return findNonSplittableRouteWithTwoConstraints(src, dst, bw, cost);
        else
            return findNonSplittableRouteWithOneConstraint(src, dst, bw);
    }

    private <T> Map<List<INode<T>>, Double> findNonSplittableRouteWithTwoConstraints(INode<T> src, INode<T> dst, double bw, double cost)
    {
        int nodesNum = routeFinder.getTopo().getKnownNodes().size();
        routeFinder.getTopo().prepareTopoForDijkstra(bw, false);

        List<Set<INode<T>>> neighborhoods = buildRestrictedNeighborhoodsWithLoops(src, dst, bw, cost, nodesNum);

        return getPathToInt(src, dst, neighborhoods.size() - 1, bw);
    }


    private <T> Map<List<INode<T>>, Double> findNonSplittableRouteWithOneConstraint(INode<T> src, INode<T> dst, double bw)
    {
        List<Set<INode<T>>> neighborhoods = buildRestrictedNeighborhoods(src, dst, bw);

        return getPathToInt(src, dst, neighborhoods.size() - 1, bw);
    }


    protected  <T> List<Set<INode<T>>> buildRestrictedNeighborhoods(INode<T> src, INode<T> dst, double bw)
    {
        List<Set<INode<T>>> neighborhoods = new ArrayList<>();
        Set<INode<T>> prevNeighborhood = new LinkedHashSet<>();
        Set<INode<T>> currentNeighborhood = Collections.singleton(src);
        neighborhoods.add(currentNeighborhood);

        while (!currentNeighborhood.contains(dst))
        {
            Set<INode<T>> newNeighborhood = new LinkedHashSet<>();

            for (INode<T> n : currentNeighborhood)
                for (Map.Entry<INode<T>, ILink> neighbor : n.getNeighbors().entrySet())
                    if (!currentNeighborhood.contains(neighbor.getKey()) && !prevNeighborhood.contains(neighbor.getKey())
                            && neighbor.getValue().getBw() >= bw)
                    {
                        newNeighborhood.add(neighbor.getKey());
                        neighbor.getKey().setNMPredecessor(neighborhoods.size(), n);
                    }

            if (newNeighborhood.isEmpty())
                throw new RuntimeException("NM can't provide path from " + src.getID() + " to " + dst.getID() + " because of constraint: bw=" + bw + " - destination unreachable!!!");

            neighborhoods.add(newNeighborhood);
            prevNeighborhood = currentNeighborhood;
            currentNeighborhood = newNeighborhood;
        }
        return neighborhoods;
    }

    protected <T> List<Set<INode<T>>> buildRestrictedNeighborhoodsWithLoops(INode<T> src, INode<T> dst, double bw, double cost, int nodesNum)
    {
        List<Set<INode<T>>> neighborhoods = new ArrayList<>();

        Set<INode<T>> currentNeighborhood = new HashSet<>();
        currentNeighborhood.add(src);
        src.setNMMinDist(0,0);

        neighborhoods.add(currentNeighborhood);

        while (!currentNeighborhood.contains(dst))
        {
            Set<INode<T>> newNeighborhood = new HashSet<>();

            for (INode<T> n : currentNeighborhood)
            {
                for (Map.Entry<INode<T>, ILink> neighbor : n.getNeighbors().entrySet())
                {
                    double newDist = n.getNMMinDist(neighborhoods.size()-1) + neighbor.getValue().getAvCost();
                    if (newDist <= cost && neighbor.getKey().getMinDist() > newDist)
                    {
                        newNeighborhood.add(neighbor.getKey());
                        neighbor.getKey().setNMMinDist(neighborhoods.size(),newDist);
                        neighbor.getKey().setNMPredecessor(neighborhoods.size(), n);
                    }
                }
            }

            if (newNeighborhood.isEmpty() || neighborhoods.size() >= nodesNum)
                throw new RuntimeException("NM can't provide path from " + src.getID() + " to " + dst.getID()
                        + " because of constraints: bw=" + bw + " &cost=" + cost + " or destination unreachable!!!");

            neighborhoods.add(newNeighborhood);
            currentNeighborhood = newNeighborhood;
        }

        if(log.isDebugEnabled())
        {
            log.debug("[NM-RouteFinder: NM] Current NHs size=" + neighborhoods.size());
            log.debug("[NM-RouteFinder: NM] NHs: " + neighborhoods);
        }

        return neighborhoods;
    }


    protected <T> Map<List<INode<T>>, Double> getPathToInt(INode<T> src, INode<T> dst, int nhSize, double bw)
    {
        List<INode<T>> path = new ArrayList<>();

        //set final distance
        dst.setMinDist(dst.getNMMinDist(nhSize));
        //backtrack
        for (INode<T> vertex = dst; vertex != src; vertex = vertex.getNMPredecessor(nhSize--))
            path.add(vertex);

        path.add(src);
        Collections.reverse(path);

        // allocate only requested resources
//        routeFinder.getRe().allocateRoute(path, bw); allocate within route finder!!!

        Map<List<INode<T>>, Double> map = new HashMap<>();
        map.put(path, bw);

        return map;
    }
}
