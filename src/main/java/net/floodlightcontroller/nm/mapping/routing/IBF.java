package net.floodlightcontroller.nm.mapping.routing;

import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;

import java.util.*;

/**
 * Created by chemo_000 on 5/9/2016.
 */
public class IBF
{
    private RouteFinder rf;

    public IBF(RouteFinder rf)
    {
        this.rf = rf;
    }

    public <T> Map<List<INode<T>>, Double> findBwDelayPath(INode<T> src, INode<T> dst, double bw, double cost)
    {
        if (cost > 0)
            return computeBwDelayPath(src, dst, bw, cost);
        else
            throw new RuntimeException("IBF currently does not support only BW constraint case," +
                    " please specify cost constraint!");
    }

    private <T> Map<List<INode<T>>, Double> computeBwDelayPath(INode<T> src, INode<T> dst, double bw, double cost)
    {
        //prune all links which does not satisfy constraints
        rf.getTopo().prepareTopoForDijkstra(bw, false);

        return computeBestDelayPath(src, dst, bw, cost);
    }

    private <T> Map<List<INode<T>>, Double> computeBestDelayPath(INode<T> src, INode<T> dst, double bw, double cost)
    {
        src.setMinDist(0);
        Set<INode<T>> nodesToRelax = new HashSet<>();
        nodesToRelax.add(src);
        int iter = 0;
        //repeat main cycle V-1 times
        while (iter < (rf.getTopo().getKnownNodes().size() - 1) && !nodesToRelax.isEmpty())
        {
            ++iter;
            Set<INode<T>> nodesToRelaxNext = new HashSet<>();
            //relax outgoing edges
            for (INode<T> n : nodesToRelax)
                for (Map.Entry<INode<T>, ILink> neighbor : n.getNeighbors().entrySet())
                {
                    double newDist = n.getMinDist() + neighbor.getValue().getAvCost();
                    if (newDist < neighbor.getKey().getMinDist())
                    {
                        neighbor.getKey().setMinDist(newDist);
                        neighbor.getKey().setPredecessor(n);
                        nodesToRelaxNext.add(neighbor.getKey());
                    }
                }

            //check if current iteration finds path with satisfied constraint
            if (dst.getMinDist() <= cost)
            {
//                    System.out.println("IBF found path for " + j + " iterations.");
                return getPathToInt(src, dst, bw);
            }

            nodesToRelax = nodesToRelaxNext;
        }
        throw new RuntimeException("IBF can't provide path from " + src.getID() + " to " + dst.getID() + " for "
                + iter + " iterations - destination unreachable!!!");
    }


    private <T> Map<List<INode<T>>, Double> getPathToInt(INode src, INode dst, double bw)
    {
        List<INode<T>> path = new ArrayList<>();

        for (INode vertex = dst; vertex != src; vertex = vertex.getPredecessor())
            path.add(vertex);

        path.add(src);
        Collections.reverse(path);

        // allocate only requested resources
//            rf.getRe().allocateRoute(path, bw);

        Map<List<INode<T>>, Double> result = new HashMap<>();
        result.put(path, bw);

        return result;
    }
}
