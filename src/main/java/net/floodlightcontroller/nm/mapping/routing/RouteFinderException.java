package net.floodlightcontroller.nm.mapping.routing;

/**
 * Created by chemo_000 on 3/10/2015.
 */
public class RouteFinderException extends RuntimeException
{
    public RouteFinderException(String errMsg)
    {
        super(errMsg);
    }

    public RouteFinderException(String errMsg, Exception ex)
    {
        super(errMsg, ex);
    }
}
