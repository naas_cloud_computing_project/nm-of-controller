package net.floodlightcontroller.nm.mapping.routing;

import net.floodlightcontroller.nm.allocation.RouteBroker;
import net.floodlightcontroller.nm.discovery.model.INode;
import net.floodlightcontroller.nm.utils.CartesianUtils;

import java.util.*;

/**
 * Created by chemo_000 on 4/2/2015.
 */
public class NonSplittableNM
{
    private CartesianUtils cu;
    private RouteFinder routeFinder;

    public NonSplittableNM(CartesianUtils cartesianUtils, RouteFinder routeFinder)
    {
        this.cu = cartesianUtils;
        this.routeFinder = routeFinder;
    }

    protected <T> Map<List<INode<T>>, Double> findNonSplittableRoute(INode<T> src, INode<T> dst, double bw)
    {
        long startPathComp = System.nanoTime();
        List<Set<INode<T>>> neighborhoods = routeFinder.getNb().buildRestrictedNeighborhoods(src, dst, bw);
        neighborhoods.set(neighborhoods.size() - 1, Collections.singleton(dst));//left only dst node at the last ring

        Set<List<INode<T>>> paths = cu.restrictedCartesianProduct(neighborhoods, bw);

        //at this point we will have guaranteed at least one suitable path (hence there is no need for path estimation)
        List<INode<T>> path = paths.iterator().next();
        System.out.println("[NM-ToN DATA] Time to VL path computation:" + (System.nanoTime()-startPathComp)/1000);
        long startPathAlloc = System.nanoTime();
        // allocate only requested resources
        routeFinder.getRe().allocateRoute(path, bw);
        System.out.println("[NM-ToN DATA] Time to VL path allocation:" + (System.nanoTime()-startPathComp)/1000);
        Map<List<INode<T>>, Double> res = new HashMap<>();
        res.put(path, bw);

        return res;
    }
}
