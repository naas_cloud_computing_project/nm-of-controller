package net.floodlightcontroller.nm.mapping.routing;

import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;
import org.teneighty.heap.FibonacciHeap;

import java.util.*;

public class Dijkstra
{
    private RouteFinder rf;

    public Dijkstra(RouteFinder rf)
    {
        this.rf = rf;
    }

    public <T> Map<List<INode<T>>, Double> findBwDelayPath(INode<T> src, INode<T> dst, double bw, double cost)
    {
        if (cost > 0)
            return computeBwDelayPath(src, dst, bw, cost, true);
        else
            return computeBwSPFPath(src, dst, bw, true);
    }

    private <T> Map<List<INode<T>>, Double> computeBwDelayPath(INode<T> src, INode<T> dst, double bw, double cost, boolean allocate)
    {
        rf.getTopo().prepareTopoForDijkstra(bw, false);

        return computeBestDelayPath(src, dst, bw, cost, allocate);
    }

    private <T> Map<List<INode<T>>, Double> computeBwSPFPath(INode<T> src, INode<T> dst, double bw, boolean allocate)
    {
        rf.getTopo().prepareTopoForDijkstra(bw, true);

        return computeShortestPath(src, dst, bw);
    }

    private <T> Map<List<INode<T>>, Double> computeBestDelayPath(INode<T> src, INode<T> dst, double bw, double cost, boolean allocate)
    {
        src.setMinDist(0);
        FibonacciHeap<INode, Double> unSettledNodes = new FibonacciHeap<>();

        unSettledNodes.insert(src, src.getMinDist());

        while (!unSettledNodes.isEmpty())
        {
            INode<T> v = unSettledNodes.extractMinimum().getKey();//takeNextNodeWithMinDistance(unSettledNodes);

            if (v.equals(dst))
            {
                if (dst.getMinDist() > cost)
                {
                    throw new RuntimeException("Dijkstra's can't find path from " + src.getID() + " to " + dst.getID()
                            + " with specified bw=" + bw + " & cost=" + cost);
                }
                return getPathToInt(src, dst, bw);
            }

            for (Map.Entry<INode<T>, ILink> entry : v.getNeighbors().entrySet())
            {
                INode n = entry.getKey();

                double distanceThroughU = v.getMinDist() + entry.getValue().getAvCost();

                if (distanceThroughU < n.getMinDist())
                {
                    n.setMinDist(distanceThroughU);
                    n.setPredecessor(v);
                    unSettledNodes.insert(n, distanceThroughU);
                }
            }
        }

        throw new RuntimeException("Dijkstra's can't provide path from " + src.getID() + " to " + dst.getID() + " - destination unreachable!!!");
    }

    private <T> Map<List<INode<T>>, Double> computeShortestPath(INode<T> src, INode<T> dst, double bw)
    {
        src.setMinDist(0);
        FibonacciHeap<INode, Double> unSettledNodes = new FibonacciHeap<>();

        unSettledNodes.insert(src, src.getMinDist());

        while (!unSettledNodes.isEmpty())
        {
            INode<T> v = unSettledNodes.extractMinimum().getKey();//takeNextNodeWithMinDistance(unSettledNodes);

            if (v.equals(dst))
                return getPathToInt(src, dst, bw);

            for (Map.Entry<INode<T>, ILink> entry : v.getNeighbors().entrySet())
            {
                INode n = entry.getKey();

                double distanceThroughU = v.getMinDist() + entry.getValue().getAvCost();

                if (distanceThroughU < n.getMinDist())
                {
                    n.setMinDist(distanceThroughU);
                    n.setPredecessor(v);
                    unSettledNodes.insert(n, distanceThroughU);
                }
            }
        }

        throw new RuntimeException("Dijkstra's can't provide path from " + src.getID() + " to " + dst.getID() + " - destination unreachable!!!");
    }


   private <T> Map<List<INode<T>>, Double> getPathToInt(INode src, INode dst, double bw)
    {
        List<INode<T>> path = new ArrayList<>();

        for (INode vertex = dst; vertex != src; vertex = vertex.getPredecessor())
            path.add(vertex);

        path.add(src);
        Collections.reverse(path);

        // allocate only requested resources
//            rf.getRe().allocateRoute(path, bw);

        Map<List<INode<T>>, Double> result = new HashMap<>();
        result.put(path, bw);

        return result;
    }

    protected <T> void computeAllCostPaths(INode<T> src)
    {
        src.setMinDist(0.0);
        FibonacciHeap<INode<T>, Double> unSettledNodes = new FibonacciHeap<>();

        unSettledNodes.insert(src, 0.0);

        while (!unSettledNodes.isEmpty())
        {
            INode<T> v = unSettledNodes.extractMinimum().getKey();

            for (Map.Entry<INode<T>, ILink> entry : v.getNeighbors().entrySet())
            {
                INode<T> n = entry.getKey();

                double distanceThroughU = v.getMinDist() + entry.getValue().getAvCost();

                if (distanceThroughU < n.getMinDist())
                {
                    n.setMinDist(distanceThroughU);
                    unSettledNodes.insert(n, distanceThroughU);
                }
            }
        }
    }
}
