package net.floodlightcontroller.nm.mapping.routing;

import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;

import java.util.*;

/**
 * Created by chemo_000 on 2/6/2015.
 */
public class NeighborhoodBuilder
{

    //Building Neighborhoods Logic goes here
    protected <T> List<Set<INode<T>>> buildNextNeighborhoods(INode<T> dst, List<Set<INode<T>>> neighborhoods, int iter)
    {
        Set<INode<T>> newNeighborhood = new HashSet<>();

        for (INode n : neighborhoods.get(neighborhoods.size() - 1))
            newNeighborhood.addAll(n.getNeighbors().keySet());

        neighborhoods.add(newNeighborhood);

        if (iter > 15)
            throw new RuntimeException("dst:" + dst.getID() + " is unreachable!"); //todo make known exception for NBuilder and proper handling

        return newNeighborhood.contains(dst) ?
                neighborhoods :
                buildNextNeighborhoods(dst, neighborhoods, ++iter);
    }

    protected <T> void addNextNeighborhood(List<Set<INode<T>>> neighborhoods)
    {
        Set<INode<T>> newNeighborhood = new HashSet<>();

        for (INode n : neighborhoods.get(neighborhoods.size() - 1))
            newNeighborhood.addAll(n.getNeighbors().keySet());

        neighborhoods.add(newNeighborhood);
    }

    protected <T> List<Set<INode<T>>> buildRestrictedNeighborhoods(INode<T> src, INode<T> dst, double bw)//todo work on performance
    {
        List<Set<INode<T>>> neighborhoods = new ArrayList<>();
        Set<INode<T>> prevNeighborhood = new LinkedHashSet<>();
        Set<INode<T>> currentNeighborhood = Collections.singleton(src);
        neighborhoods.add(currentNeighborhood);

        while (!currentNeighborhood.contains(dst))
        {
            Set<INode<T>> newNeighborhood = new LinkedHashSet<>();//todo check using LinkedHashSet

            for (INode<T> n : currentNeighborhood)
                for (Map.Entry<INode<T>, ILink> neighbor : n.getNeighbors().entrySet())
                    if (!currentNeighborhood.contains(neighbor.getKey()) && !prevNeighborhood.contains(neighbor.getKey()) && neighbor.getValue().getBw() >= bw) //!knownNodes.contains(neighbor.getKey()) &&
                        newNeighborhood.add(neighbor.getKey());

            if (newNeighborhood.isEmpty())
                throw new RuntimeException("NM can't provide path from " + src.getID() + " to " + dst.getID() + " because of constraint: bw=" + bw + " - destination unreachable!!!");

            neighborhoods.add(newNeighborhood);
            prevNeighborhood = currentNeighborhood;
            currentNeighborhood = newNeighborhood;
        }
        return neighborhoods;
    }
}
