package net.floodlightcontroller.nm;

import net.floodlightcontroller.nm.xml.ConfigParser;

/**
 * Created by chemo_000 on 3/10/2015.
 */
public interface NMConstants
{
    //ratio between queue id and bw, i.e. queue id=1 and ratio=5 mean queue supports 5 Mbps max rate
    public static int BW_TO_QUEUE_ID_RATIO = ConfigParser.getInstance().BW_TO_QUEUE_ID_RATIO;
    //max bw for host-to-switch links
    public static double MAX_HOST_LINK_BW = ConfigParser.getInstance().MAX_HOST_LINK_BW;
    //max bw for Switch-to-switch links
    public static double MAX_SWITCH_LINK_BW = ConfigParser.getInstance().MAX_SWITCH_LINK_BW;
    //idle timeout of the flow, i.e., how long flow will remain in table since last matching before be deleted
    public static int FLOW_IDLE_TIMEOUT = ConfigParser.getInstance().FLOW_IDLE_TIMEOUT;
    //hard timeout of the flow, i.e., how long flow will remain in table before be deleted (last matching does not matter)
    public static int FLOW_HARD_TIMEOUT = ConfigParser.getInstance().FLOW_HARD_TIMEOUT;
}
