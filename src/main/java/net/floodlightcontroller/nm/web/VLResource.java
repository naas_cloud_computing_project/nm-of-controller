package net.floodlightcontroller.nm.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.nm.allocation.RouteBroker;
import net.floodlightcontroller.nm.openflow.customservices.IRouteFinderService;
import net.floodlightcontroller.nm.openflow.customservices.impl.VLHolder;
import net.floodlightcontroller.nm.utils.LogWriter;
import net.floodlightcontroller.nm.web.model.VirtualLink;
import net.floodlightcontroller.nm.xml.ConfigParser;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import java.io.IOException;
import java.util.Collections;
import java.util.Set;

/**
 * Created by chemo_000 on 4/23/2015.
 */
public class VLResource extends ServerResource implements IJsonVL
{
    @Get("json")
    public Set<VirtualLink> retrieve()
    {
        String user = getAttribute("user");
        Set<VirtualLink> res = null;

        if (user != null)
            res = VLHolder.getInstance().getUserVL(user);

        if (res != null)
            return res;
        else
            return Collections.EMPTY_SET;
    }

    @Post("json")
    public String allocateVL(String json)
    {
        IStaticFlowEntryPusherService flowPusher =
                (IStaticFlowEntryPusherService) getContext().getAttributes().
                        get(IStaticFlowEntryPusherService.class.getCanonicalName());

        IRouteFinderService routeFinder =
                (IRouteFinderService) getContext().getAttributes().
                        get(IRouteFinderService.class.getCanonicalName());

        if (RouteBroker.getInstance().needFlowPusher())
            RouteBroker.getInstance().setFlowPusherService(flowPusher);

        String user = getAttribute("user");

        if (user != null)
            try
            {
                Set<VirtualLink> vls = jsonToVL(json);
                for (VirtualLink vl : vls)
                    try
                    {
                        long startPathQuery = System.nanoTime();
                        //todo put all VL to process queue; store found paths
                        routeFinder.findRoutesAndBwCost(vl.getSrc(), vl.getDst(), false, vl.getBw(), vl.getCost());

                        vl.setStatus(VirtualLink.Status.ALLOCATED);
                        if (ConfigParser.getInstance().EXPORT_RESULTS)
                            LogWriter.getInstance().logInfoToFile((System.nanoTime() - startPathQuery) / 1000 + "\n");
                    } catch (Exception ex)
                    {
                        vl.setStatus(VirtualLink.Status.FAILED);
                        vl.setError(ex.getMessage());
                        ex.printStackTrace();
                    }

                VLHolder.getInstance().addUserVL(user, vls);
                return vlToJson(vls);
            } catch (IOException ex)
            {
                ex.printStackTrace();
                return ("{\"ERROR\" : \"exception during json parsing: " + ex.getMessage() + "\"}");
            }
        else
            return ("{\"ERROR\" : \"user=null\"}");
    }

    public static Set<VirtualLink> jsonToVL(String json) throws IOException
    {
        System.out.println("Next JSON obtained: " + json);
        ObjectMapper mapper = new ObjectMapper();
        //deserialize JSON objects as a Set
        Set<VirtualLink> vls = mapper.readValue(json,
                mapper.getTypeFactory().constructCollectionType(Set.class, VirtualLink.class));
        return vls;
    }

    public static String vlToJson(Set<VirtualLink> vls) throws JsonProcessingException
    {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(vls);

        return json;
    }
}
