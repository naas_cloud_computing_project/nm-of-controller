package net.floodlightcontroller.nm.web;


import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.nm.openflow.customservices.impl.VLHolder;
import net.floodlightcontroller.nm.web.model.VirtualLink;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import java.util.Set;

/**
 * Created by chemo_000 on 4/23/2015.
 */
public class RemoveVLsResource extends ServerResource
{
    @Get
    public void retrieve()
    {
        String user = getAttribute("user");
        if (user != null && user.equals("admin"))
        {
            VLHolder.getInstance().removeAllVLs();

            IStaticFlowEntryPusherService flowPusher =
                    (IStaticFlowEntryPusherService) getContext().getAttributes().
                            get(IStaticFlowEntryPusherService.class.getCanonicalName());

            flowPusher.deleteAllFlows();
        }
    }
}
