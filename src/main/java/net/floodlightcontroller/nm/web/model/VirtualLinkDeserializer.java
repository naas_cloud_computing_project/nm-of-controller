package net.floodlightcontroller.nm.web.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import net.floodlightcontroller.nm.web.IJsonVL;

import java.io.IOException;

/**
 * Created by chemo_000 on 4/27/2015.
 */
public class VirtualLinkDeserializer extends JsonDeserializer<VirtualLink> implements IJsonVL
{
    @Override
    public VirtualLink deserialize(JsonParser jp, DeserializationContext dC) throws IOException, JsonProcessingException
    {
        String src = null;
        String dst = null;
        double bw = -1.0;
        double cost = -1.0;
        VirtualLink.Status status = null;
//        String error="";

        if (jp.getCurrentToken() != JsonToken.START_OBJECT)
        {
            throw new IOException("Expected START_OBJECT");
        }

        while (jp.nextToken() != JsonToken.END_OBJECT)
        {
            if (jp.getCurrentToken() != JsonToken.FIELD_NAME)
            {
                throw new IOException("Expected FIELD_NAME");
            }

            String n = jp.getCurrentName();
            jp.nextToken();
            if (jp.getText().equals(""))
            {
                continue;
            }

            switch (n)
            {
                case SRC:
                    src = jp.getText();
                    break;
                case DST:
                    dst = jp.getText();
                    break;
                case BW:
                    bw = jp.getDoubleValue();
                    break;
                case COST:
                    cost = jp.getDoubleValue();
                    break;
                case STATUS:
                    status = VirtualLink.Status.valueOf(jp.getText());
                    break;
//                case ERROR:
//                    error = jp.getText();
                default:
                    break;
            }
        }

        if (src != null && dst != null && bw >= 0 && cost >= 0 && status != null)
            return new VirtualLink(src, dst, bw, cost, status);
        else throw new IOException("wrong VL parameters");
    }
}
