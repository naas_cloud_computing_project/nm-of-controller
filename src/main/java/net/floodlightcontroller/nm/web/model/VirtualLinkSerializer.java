package net.floodlightcontroller.nm.web.model;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import net.floodlightcontroller.nm.web.IJsonVL;

import java.io.IOException;

/**
 * Created by chemo_000 on 4/27/2015.
 */
public class VirtualLinkSerializer extends JsonSerializer<VirtualLink> implements IJsonVL
{
    @Override
    public void serialize(VirtualLink vl, JsonGenerator jgen, SerializerProvider arg) throws IOException, JsonProcessingException
    {
        jgen.writeStartObject();
        jgen.writeStringField(SRC, vl.src);
        jgen.writeStringField(DST, vl.dst);
        jgen.writeNumberField(BW, vl.bw);
        jgen.writeNumberField(COST, vl.cost);
        jgen.writeStringField(STATUS, vl.status.toString());
        jgen.writeStringField(ERROR, vl.error);
        jgen.writeEndObject();
    }
}
