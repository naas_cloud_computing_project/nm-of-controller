package net.floodlightcontroller.nm.web.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import net.floodlightcontroller.nm.discovery.model.INode;

/**
 * Created by chemo_000 on 4/29/2015.
 */
@JsonSerialize(using = PhysicalLinkSerializer.class)
@JsonDeserialize(using = PhysicalLinkDeserializer.class)
public class PhysicalLink
{
    protected String src;
    protected INode.INodeType srcType;
    protected String dst;
    protected INode.INodeType dstType;
    protected double avBw = 0;
    protected double capacity = 0;
    protected double cost = 0;

    public PhysicalLink()
    {
    }

    public PhysicalLink(String src, INode.INodeType srcType, String dst,
                        INode.INodeType dstType, double avBw, double capacity, double cost)
    {
        this.src=src;
        this.srcType=srcType;
        this.dst=dst;
        this.dstType=dstType;
        this.avBw=avBw;
        this.capacity=capacity;
        this.cost=cost;
    }
}
