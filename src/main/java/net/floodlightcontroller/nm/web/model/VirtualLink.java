package net.floodlightcontroller.nm.web.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Created by chemo_000 on 4/23/2015.
 */
@JsonSerialize(using = VirtualLinkSerializer.class)
@JsonDeserialize(using = VirtualLinkDeserializer.class)
public class VirtualLink
{
    protected String src;
    protected String dst;
    protected double bw = 0;
    protected double cost = 0;
    protected Status status = Status.FAILED;
    protected String error = "";

    public enum Status
    {
        ALLOCATED, FAILED, PENDING
    }

    public VirtualLink()
    {
    }

    public VirtualLink(String src, String dst, double bw, double cost, Status status)
    {
        this.src = src;
        this.dst = dst;
        this.bw = bw;
        this.cost = cost;
        this.status = status;
    }

    public String getSrc()
    {
        return src;
    }

    public String getDst()
    {
        return dst;
    }

    public double getBw()
    {
        return bw;
    }

    public double getCost()
    {
        return cost;
    }

    public Status getStatus()
    {
        return status;
    }

    public String getError()
    {
        return error;
    }

    public void setStatus(Status status)
    {
        this.status = status;
    }

    public void setError(String error)
    {
        this.error = error;
    }
}
