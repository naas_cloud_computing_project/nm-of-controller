package net.floodlightcontroller.nm.web.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import net.floodlightcontroller.nm.discovery.model.INode;
import net.floodlightcontroller.nm.web.IJsonVL;

import java.io.IOException;

/**
 * Created by chemo_000 on 4/29/2015.
 */
public class PhysicalLinkDeserializer extends JsonDeserializer<PhysicalLink> implements IJsonVL
{
    @Override
    public PhysicalLink deserialize(JsonParser jp, DeserializationContext dC) throws IOException, JsonProcessingException
    {
        String src = null;
        INode.INodeType srcType = null;
        String dst = null;
        INode.INodeType dstType = null;
        double avBw = -1.0;
        double capacity = -1.0;
        double cost = -1.0;

        if (jp.getCurrentToken() != JsonToken.START_OBJECT)
        {
            throw new IOException("Expected START_OBJECT");
        }

        while (jp.nextToken() != JsonToken.END_OBJECT)
        {
            if (jp.getCurrentToken() != JsonToken.FIELD_NAME)
            {
                throw new IOException("Expected FIELD_NAME");
            }

            String n = jp.getCurrentName();
            jp.nextToken();
            if (jp.getText().equals(""))
            {
                continue;
            }

            switch (n)
            {
                case SRC:
                    src = jp.getText();
                    break;
                case SRC_TYPE:
                    srcType = INode.INodeType.valueOf(jp.getText());
                    break;
                case DST:
                    dst = jp.getText();
                    break;
                case DST_TYPE:
                    dstType = INode.INodeType.valueOf(jp.getText());
                    break;
                case BW:
                    avBw = jp.getDoubleValue();
                    break;
                case CAPACITY:
                    capacity = jp.getDoubleValue();
                    break;
                case COST:
                    cost = jp.getDoubleValue();
                    break;
                default:
                    break;
            }
        }

        if (src != null && srcType!=null && dst != null && dstType!=null && avBw >= 0 && capacity >=0 && cost >=0)
            return new PhysicalLink(src, srcType, dst, dstType, avBw, capacity, cost);
        else throw new IOException("wrong PL parameters");
    }
}
