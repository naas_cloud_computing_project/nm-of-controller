package net.floodlightcontroller.nm.web;

/**
 * Created by chemo_000 on 4/27/2015.
 */
public interface IJsonVL
{
    public final String SRC = "source";
    public final String SRC_TYPE = "src_type";
    public final String DST = "destination";
    public final String DST_TYPE = "dst_type";
    public final String BW = "bandwidth";
    public final String CAPACITY = "capacity";
    public final String COST = "cost";
    public final String STATUS = "status";
    public final String ERROR = "error";
}
