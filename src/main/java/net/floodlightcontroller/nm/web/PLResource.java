package net.floodlightcontroller.nm.web;

import net.floodlightcontroller.linkdiscovery.ILinkDiscoveryService;
import net.floodlightcontroller.routing.Link;
import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.nm.NMConstants;
import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;
import net.floodlightcontroller.nm.discovery.model.ITopology;
import net.floodlightcontroller.nm.openflow.customservices.IRouteFinderService;
import net.floodlightcontroller.nm.openflow.customservices.impl.HostHolderImpl;
import net.floodlightcontroller.nm.utils.AllocatedBwEstimator;
import net.floodlightcontroller.nm.web.model.PhysicalLink;
import net.floodlightcontroller.nm.xml.LinkConfigParser;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.projectfloodlight.openflow.types.DatapathId;
import org.projectfloodlight.openflow.types.IPv4Address;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import java.util.*;

/**
 * Created by chemo_000 on 4/29/2015.
 */
public class PLResource extends ServerResource implements IJsonVL
{
    @Get("json")
    public Set<PhysicalLink> retrieve()
    {
//        ILinkDiscoveryService linkService = (ILinkDiscoveryService) getContext().getAttributes()
//                .get(ILinkDiscoveryService.class.getCanonicalName());
        String user = getAttribute("user");
        if (user != null && user.equals("admin"))
        {
            IRouteFinderService routeFinder =
                    (IRouteFinderService) getContext().getAttributes().
                            get(IRouteFinderService.class.getCanonicalName());

            ITopology t = routeFinder.getTopo();
            t.updateTopoCache();

//            AllocatedBwEstimator bwEstimator = new AllocatedBwEstimator();
            //first collect info about switch-to-switch links
//            Set<PhysicalLink> pls = convertSwLinksToPhysicalLinks(getSwitchesLinks(linkService), bwEstimator);
            //then collect info about all known host links
//            pls.addAll(convertHostLinksToPhysicalLinks(bwEstimator));
            Set<PhysicalLink> pls = convertKnownLinksToPLs(t.getKnownLinks());

            return pls;
        } else return Collections.EMPTY_SET;
    }

    private Set<PhysicalLink> convertKnownLinksToPLs(Map<Pair<INode,INode>, ILink> links)
    {
        Set<PhysicalLink> pls = new HashSet<>();

        for (Map.Entry<Pair<INode,INode>, ILink> e : links.entrySet())
        {
            INode srcNode = e.getKey().getLeft();
            String src = srcNode.getID().toString();
            INode.INodeType srcType = srcNode.getType();

            INode dstNode = e.getKey().getRight();
            String dst = dstNode.getID().toString();
            INode.INodeType dstType = dstNode.getType();

            Pair<String, String> linkKey = new ImmutablePair<>(src, dst);
            Double Capacity = LinkConfigParser.getInstance().getLinkCapacity(linkKey);
            double capacity = Capacity == null ? NMConstants.MAX_HOST_LINK_BW : Capacity;
            double avBw = e.getValue().getBw();
            double cost = e.getValue().getCost();

            pls.add(new PhysicalLink(src, srcType, dst, dstType, avBw, capacity, cost));
        }

        return pls;
    }

    private Collection<? extends PhysicalLink> convertHostLinksToPhysicalLinks(AllocatedBwEstimator bwEstimator)
    {
        Set<PhysicalLink> pls = new LinkedHashSet<>();

        IStaticFlowEntryPusherService flowPusher =
                (IStaticFlowEntryPusherService) getContext().getAttributes().
                        get(IStaticFlowEntryPusherService.class.getCanonicalName());

        Map<Pair<IPv4Address, DatapathId>, Short> knownHosts =
                HostHolderImpl.getInstance().getHostsAndSwitchesToPortsMap();

        for (Map.Entry<Pair<IPv4Address, DatapathId>, Short> entry : knownHosts.entrySet())
        {
            String src = entry.getKey().getLeft().toString();
            INode.INodeType srcType = INode.INodeType.HOST;
            String dst = entry.getKey().getRight().toString();
            INode.INodeType dstType = INode.INodeType.SWITCH;
            Pair<String, String> linkKey = new ImmutablePair<>(src, dst);
            Double Capacity = LinkConfigParser.getInstance().getLinkCapacity(linkKey);
            double capacity = Capacity == null ? NMConstants.MAX_HOST_LINK_BW : Capacity;
            double avBw = capacity - bwEstimator.estimateAllocatedBw(entry.getKey().getRight(), entry.getValue(), flowPusher);
            Double Cost = LinkConfigParser.getInstance().getLinkCost(linkKey);
            double cost = Cost == null ? 0.0 : Cost;

            pls.add(new PhysicalLink(src, srcType, dst, dstType, avBw, capacity, cost));
        }

        return pls;
    }

    private Set<PhysicalLink> convertSwLinksToPhysicalLinks(Set<Link> switchesLinks, AllocatedBwEstimator bwEstimator)
    {
        Set<PhysicalLink> pls = new LinkedHashSet<>();

        IStaticFlowEntryPusherService flowPusher =
                (IStaticFlowEntryPusherService) getContext().getAttributes().
                        get(IStaticFlowEntryPusherService.class.getCanonicalName());

        for (Link l : switchesLinks)
        {
            String src = l.getSrc().toString();
            INode.INodeType srcType = INode.INodeType.SWITCH;
            String dst = l.getDst().toString();
            INode.INodeType dstType = INode.INodeType.SWITCH;
            Pair<String, String> linkKey = new ImmutablePair<>(src, dst);
            Double Capacity = LinkConfigParser.getInstance().getLinkCapacity(linkKey);
            double capacity = Capacity == null ? NMConstants.MAX_SWITCH_LINK_BW : Capacity;
            double avBw = capacity - bwEstimator.estimateAllocatedBw(l.getSrc(), l.getSrcPort().getShortPortNumber(), flowPusher);
            Double Cost = LinkConfigParser.getInstance().getLinkCost(linkKey);
            double cost = Cost == null ? 0.0 : Cost;
            pls.add(new PhysicalLink(src, srcType, dst, dstType, avBw, capacity, cost));
        }

        return pls;
    }

    private Set<Link> getSwitchesLinks(ILinkDiscoveryService linkService)
    {
        Set<Link> links = new LinkedHashSet<>();
        Set<Pair<DatapathId, DatapathId>> knownSwitches = new LinkedHashSet<>();

        for (Map.Entry<DatapathId, Set<Link>> entry : linkService.getSwitchLinks().entrySet())
            for (Link l : entry.getValue())
            {
                Pair<DatapathId, DatapathId> linkKey1 = new ImmutablePair<>(l.getSrc(), l.getDst());
                Pair<DatapathId, DatapathId> linkKey2 = new ImmutablePair<>(l.getDst(), l.getSrc());
                if (!knownSwitches.contains(linkKey1) && !knownSwitches.contains(linkKey2))
                {
                    links.add(l);

                    knownSwitches.add(linkKey1);
//                    knownSwitches.add(linkKey2);
                }
            }
        return links;
    }

}
