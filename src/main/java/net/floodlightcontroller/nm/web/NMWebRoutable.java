package net.floodlightcontroller.nm.web;

import net.floodlightcontroller.restserver.RestletRoutable;
import org.restlet.Context;
import org.restlet.Restlet;
import org.restlet.routing.Router;

/**
 * Created by chemo_000 on 4/23/2015.
 */
public class NMWebRoutable implements RestletRoutable
{
    @Override
    public Restlet getRestlet(Context context)
    {
        Router router = new Router(context);
        router.attach("/VL/{user}", VLResource.class);
        router.attach("/Remove_all_VLs/{user}", RemoveVLsResource.class);
        router.attach("/PL/{user}", PLResource.class);
        return router;
    }

    @Override
    public String basePath()
    {
        return "/wm/nm";
    }
}
