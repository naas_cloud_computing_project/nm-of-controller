package net.floodlightcontroller.nm.discovery.model;

import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.core.module.IFloodlightService;
import net.floodlightcontroller.devicemanager.IDevice;
import net.floodlightcontroller.linkdiscovery.ILinkDiscoveryService;
import org.apache.commons.lang3.tuple.Pair;
import org.projectfloodlight.openflow.types.OFPort;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Created by chemo_000 on 2/6/2015.
 */
public interface ITopology
{
    public <T> INode<T> getNodeWithID (T id);

    public void setContext(FloodlightModuleContext context);

    public void updateTopoCache();

    void prepareTopoForDijkstra(double bw, boolean isSPF);

    public Collection<INode> getKnownNodes();

    public Map<Pair<INode,INode>, ILink> getKnownLinks();
}
