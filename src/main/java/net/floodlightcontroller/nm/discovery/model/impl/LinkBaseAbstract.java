package net.floodlightcontroller.nm.discovery.model.impl;

import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.utils.AllocatedBwEstimator;
import org.projectfloodlight.openflow.types.DatapathId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by chemo_000 on 3/10/2015.
 */
public abstract class LinkBaseAbstract implements ILink
{
    protected static Logger log = LoggerFactory.getLogger(HostNodeBase.class);

    private TopologyBase topology;

    private double avCost = Double.POSITIVE_INFINITY; //used by Dijkstrsa like algorithms to allow link pruning by setting up an infinite cost of going through it

    public LinkBaseAbstract(TopologyBase topology)
    {
        this.topology = topology;
    }

    protected double calculateAllocatedBwForPort(DatapathId dpid, short port)
    {
        return new AllocatedBwEstimator().estimateAllocatedBw(dpid, port, topology.getFlowPusher());
    }

    public double getAvCost()
    {
        return avCost;
    }

    public void setAvCost(double cost)
    {
        this.avCost = cost;
    }
}
