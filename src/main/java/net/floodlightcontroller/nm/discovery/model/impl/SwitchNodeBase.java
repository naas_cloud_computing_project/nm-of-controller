package net.floodlightcontroller.nm.discovery.model.impl;

import net.floodlightcontroller.linkdiscovery.ILinkDiscovery;
import net.floodlightcontroller.linkdiscovery.ILinkDiscoveryService;
import net.floodlightcontroller.linkdiscovery.LinkInfo;
import net.floodlightcontroller.routing.Link;
import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;
import org.apache.commons.lang3.tuple.Pair;
import org.projectfloodlight.openflow.types.DatapathId;
import org.projectfloodlight.openflow.types.IPv4Address;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by chemo_000 on 2/13/2015.
 */
public class SwitchNodeBase<T> extends NodeBaseAbstract
{
    public SwitchNodeBase(T id, TopologyBase topology)
    {
        super(id, topology);
    }

    @Override
    public INodeType getType()
    {
        return INodeType.SWITCH;
    }

    @Override
    public Map<INode<T>, ILink> getNeighbors()//todo refactor
    {
        if (!neighbors.isEmpty())
            return neighbors;
        else if (id instanceof String)
        {
            Map<INode<T>, ILink> neighbors = new HashMap<>();
            ILinkDiscoveryService linkDiscoveryService = topology.getLinkDiscoveryService();
            DatapathId swDPID = DatapathId.of((String) id);

            Map<DatapathId, Set<Link>> swithLinks = linkDiscoveryService.getSwitchLinks();
            if (swithLinks != null && swithLinks.get(swDPID) != null) //todo make correct checking of switch links!!!
                for (Link swLink : swithLinks.get(swDPID))
                {
                    LinkInfo lInfo = linkDiscoveryService.getLinkInfo(swLink);
                    if (lInfo.getLinkType() != ILinkDiscovery.LinkType.INVALID_LINK
                            && (swLink.getDst() != null && swLink.getSrc() != null))//todo add check for QoS guarantees in future

                    {
                        DatapathId neighborDPID = swLink.getSrc().equals(swDPID) ? swLink.getDst() : swLink.getSrc();

                        INode<T> neighbor = new SwitchNodeBase<>(neighborDPID.toString(), topology);
                        ILink link = new SwitchLinkBase(topology, swLink);

                        if (!neighbors.containsKey(neighbor))
                        {
                            neighbors.put(neighbor, link);

                            if (log.isTraceEnabled())
                                log.trace("[NM-model] new neighbor: " + neighbor + " was discovered with link:" + link);
                        }
                    }
                }
            //add all known host neighbors
            addHostNeighbors(neighbors, swDPID);
            return neighbors;
        }

        throw new UnsupportedOperationException("[NM-model]" + getID().getClass() + " type of node ID currently isn't supported!");
    }

    private <T> void addHostNeighbors(Map<INode<T>, ILink> neighbors, DatapathId dpid)//todo here is assumption that one host can connect only to one border switch!!!
    {
        for (Pair<IPv4Address, Short> hostAndPort : topology.getSwitchHostsAndPorts(dpid))
        {
            String hostIP = hostAndPort.getLeft().toString();
            short port = hostAndPort.getRight();

            INode<T> host = new HostNodeBase<>((T) hostIP, (T) getID(), port, topology);
            ILink hostLink = new HostLinkBase(topology, host.getID().toString(), dpid, port);

            neighbors.put(host, hostLink);
        }
    }
}
