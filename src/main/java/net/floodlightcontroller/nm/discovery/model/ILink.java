package net.floodlightcontroller.nm.discovery.model;

import net.floodlightcontroller.nm.NMConstants;

/**
 * Created by chemo_000 on 2/6/2015.
 */
public interface ILink extends NMConstants
{
    public double getBw();

    public double getCost();

    public <T> short getSrcPort(INode<T> src, INode<T> dst);

    public double getAvCost();

    public void setAvCost(double cost);
}
