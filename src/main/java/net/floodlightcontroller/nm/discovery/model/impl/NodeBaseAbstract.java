package net.floodlightcontroller.nm.discovery.model.impl;

import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by chemo_000 on 2/13/2015.
 */
public abstract class NodeBaseAbstract<T> implements INode
{
    protected static Logger log = LoggerFactory.getLogger(HostNodeBase.class);

    protected T id;
    protected TopologyBase topology;
    protected Map<INode, ILink> neighbors;
    private double minDist = Double.POSITIVE_INFINITY;
    private INode predecessor = null;
    private Map<Integer, INode> predecessors;
    private Map<Integer, Double> minDists;
    private Map<List<INode<T>>, Double> dominantPaths;

    public NodeBaseAbstract(T id, TopologyBase topology)
    {
        this.id = id;
        this.topology = topology;
        this.neighbors = new LinkedHashMap<>();
        this.predecessors = new LinkedHashMap<>();
        this.minDists = new LinkedHashMap<>();
        this.dominantPaths = new LinkedHashMap<>();

        if (log.isTraceEnabled())
            log.trace("[NM-model]" + this.toString() + "was created:");
    }

    @Override
    public T getID()
    {
        return id;
    }

    public String toString()
    {
        return getType() + " Node id=" + getID();
    }

    public boolean equals(Object o)
    {
        if (o != null && o instanceof INode)
        {
            INode n = (INode) o;
            if (this.getID().equals(n.getID()) && this.getType().equals(((INode) o).getType()))
                return true;
        }
        return false;
    }

    @Override
    public int hashCode()
    {
        return this.toString().hashCode();
    }

    @Override
    public void addNeighbor(INode n, ILink l)
    {
        neighbors.put(n, l);
    }

    @Override
    public void clearNeighbors()
    {
        neighbors.clear();
    }

    @Override
    public void setMinDist(double d)
    {
        this.minDist = d;
    }

    @Override
    public void setPredecessor(INode p)
    {
        this.predecessor = p;
    }

    @Override
    public double getMinDist()
    {
        return this.minDist;
    }

    @Override
    public INode getPredecessor()
    {
        return this.predecessor;
    }

    @Override
    public int compareTo(Object other)
    {
        INode otherNode = (INode) other;
        return Double.compare(this.getMinDist(), otherNode.getMinDist());
    }

    @Override
    public INode getNMPredecessor(int nh)
    {
        return predecessors.get(nh);
    }

    @Override
    public void setNMPredecessor(int nh, INode p)
    {
        this.predecessors.put(nh, p);
    }

    @Override
    public void clearAllPredecessorsAndMinDists()
    {
        this.predecessors.clear();
        this.minDists.clear();
    }

    @Override
    public void setNMMinDist(int nh, double d)
    {
        this.minDists.put(nh, d);
    }

    @Override
    public double getNMMinDist(int nh)
    {
        if (minDists.containsKey(nh))
            return this.minDists.get(nh);
        else
            return Double.POSITIVE_INFINITY;
    }

    @Override
    public Set<List<INode<T>>> addDominantPath(List newPath, double newCostDist)
    {
        Set<List<INode<T>>> toRemove = new HashSet<>();

        if (!dominantPaths.containsKey(newPath))
        {
            for (Map.Entry<List<INode<T>>, Double> entry_path : dominantPaths.entrySet())
            {
                double pathDistance = entry_path.getValue();
                //if old path is strictly dominated by the new one remove it
                if (pathDistance > newCostDist)
                    toRemove.add(entry_path.getKey());
            }

            //add new path to processing queue as it is non dominated by others
            dominantPaths.put(newPath, newCostDist);

            //remove dominated paths from node's path queue
            for (List<INode<T>> path : toRemove)
                dominantPaths.remove(path);
        }
        return toRemove;
    }

    @Override
    public boolean isDominatedByNodePaths(List newPath, double newCostDist)
    {
        if (!dominantPaths.containsKey(newPath))
            for (double pathDistance : dominantPaths.values())
            {
                //if path is strictly dominated return true
                if (pathDistance < newCostDist)
                    return true;
            }

        return false;
    }

    @Override
    public void setDominantPaths(Map paths)
    {
        this.dominantPaths = (Map<List<INode<T>>, Double>) paths;
    }

    @Override
    public void clearDominantPaths()
    {
        this.dominantPaths.clear();
    }
}
