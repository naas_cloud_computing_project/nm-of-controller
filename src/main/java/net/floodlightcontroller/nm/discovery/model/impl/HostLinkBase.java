package net.floodlightcontroller.nm.discovery.model.impl;

import net.floodlightcontroller.nm.discovery.model.INode;
import net.floodlightcontroller.nm.xml.LinkConfigParser;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.projectfloodlight.openflow.types.DatapathId;

/**
 * Created by chemo_000 on 2/15/2015.
 */
public class HostLinkBase extends LinkBaseAbstract
{
    private short port;
    private String hostIP;
    private DatapathId dpid;

    HostLinkBase(TopologyBase topology, String hostIP, DatapathId dpid, short port)
    {
        super(topology);

        this.dpid = dpid;
        this.port = port;
        this.hostIP = hostIP;
    }

    @Override
    public double getBw()
    {
        Pair<String, String> key = new ImmutablePair<>(hostIP, dpid.toString());
        Double capacity = LinkConfigParser.getInstance().getLinkCapacity(key);
        //we need estimate available bandwidth only on switch port
        return (capacity == null ? MAX_HOST_LINK_BW : capacity) - calculateAllocatedBwForPort(dpid, port);
    }

    @Override
    public double getCost()
    {
        Pair<String, String> key = new ImmutablePair<>(hostIP, dpid.toString());
        Double cost = LinkConfigParser.getInstance().getLinkCost(key);
        return cost == null ? 0 : cost;
    }

    @Override
    public <T> short getSrcPort(INode<T> src, INode<T> dst)
    {
        if (src.getType() == INode.INodeType.SWITCH)
            return port;
        else
            return 0;
    }
}
