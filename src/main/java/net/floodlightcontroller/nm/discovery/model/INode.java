package net.floodlightcontroller.nm.discovery.model;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by chemo_000 on 2/6/2015.
 */
public interface INode<T> extends Comparable<INode>
{
    public enum INodeType{HOST, SWITCH}

    public T getID();

    public INodeType getType();

    public Map<INode<T>, ILink> getNeighbors();

    public void addNeighbor(INode n, ILink l);

    public void clearNeighbors();

    public void setMinDist(double d);

    public void setPredecessor(INode p);

    public double getMinDist();

    public INode getPredecessor();

    public INode getNMPredecessor(int nh);

    public void setNMPredecessor(int nh, INode p);

    public void clearAllPredecessorsAndMinDists();

    public void setNMMinDist(int nh, double d);

    public double getNMMinDist(int nh);

    void setDominantPaths(Map<List<INode<T>>, Double> paths);

    boolean isDominatedByNodePaths(List<INode<T>> newPath, double newCostDist);

    Set<List<INode<T>>> addDominantPath(List<INode<T>> newPath, double newCostDist);

    public void clearDominantPaths();
}
