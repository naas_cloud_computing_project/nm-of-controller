package net.floodlightcontroller.nm.discovery.model.impl;

import net.floodlightcontroller.devicemanager.IDevice;
import net.floodlightcontroller.devicemanager.SwitchPort;
import net.floodlightcontroller.linkdiscovery.ILinkDiscovery;
import net.floodlightcontroller.linkdiscovery.ILinkDiscoveryService;
import net.floodlightcontroller.linkdiscovery.LinkInfo;
import net.floodlightcontroller.routing.Link;
import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;
import net.floodlightcontroller.nm.discovery.model.ITopology;
import org.projectfloodlight.openflow.types.DatapathId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chemo_000 on 2/10/2015.
 */
public class HostNodeBase<T> extends NodeBaseAbstract
{
    private T switchID;
    private short port;

    public HostNodeBase(T id, T switchID, short port, TopologyBase topology)
    {
        super(id, topology);

        this.switchID = switchID;
        this.port = port;
    }

    @Override
    public INodeType getType()
    {
        return INodeType.HOST;
    }

    @Override
    public Map<INode<T>, ILink> getNeighbors()
    {
        if (!neighbors.isEmpty())
            return neighbors;
        else
        {

            Map<INode<T>, ILink> neighbors = new HashMap<>();

            if (switchID instanceof String)
            {
                INode<T> directSwitch = new SwitchNodeBase<>(switchID, topology);

                ILink hostLink = new HostLinkBase(topology, this.getID().toString(), DatapathId.of((String) switchID), port);

                neighbors.put(directSwitch, hostLink);
            }

            if (log.isTraceEnabled())
                log.trace("[NM-model]" + this + " has next neighbors" + neighbors);

            return neighbors;
        }
    }
}
