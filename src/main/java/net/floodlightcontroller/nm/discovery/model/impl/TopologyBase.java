package net.floodlightcontroller.nm.discovery.model.impl;


import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.linkdiscovery.ILinkDiscovery;
import net.floodlightcontroller.linkdiscovery.ILinkDiscoveryService;

import net.floodlightcontroller.linkdiscovery.LinkInfo;
import net.floodlightcontroller.nm.xml.ConfigParser;
import net.floodlightcontroller.nm.xml.LinkConfigParser;
import net.floodlightcontroller.routing.Link;
import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.ITopology;
import net.floodlightcontroller.nm.discovery.model.INode;
import net.floodlightcontroller.nm.openflow.customservices.IHostHolderService;
import net.floodlightcontroller.nm.openflow.customservices.impl.HostHolderImpl;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.projectfloodlight.openflow.types.DatapathId;
import org.projectfloodlight.openflow.types.IPv4Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by chemo_000 on 2/10/2015.
 */
public class TopologyBase implements ITopology
{
    protected static Logger log = LoggerFactory.getLogger(ITopology.class);
    private ILinkDiscoveryService linkDiscoveryService;
    private IStaticFlowEntryPusherService flowPusher;
    private IHostHolderService hostHolder;
    private Map<Object, INode> knownNodes;
    private Map<Pair<INode, INode>, ILink> knownLinks;
    private FloodlightModuleContext context;

    public TopologyBase()
    {
        this.hostHolder = HostHolderImpl.getInstance();
        this.knownNodes = new HashMap<>();
        this.knownLinks = new HashMap<>();
    }

    public void setContext(FloodlightModuleContext context)
    {
        this.context = context;
        this.linkDiscoveryService = context.getServiceImpl(ILinkDiscoveryService.class);
        this.flowPusher = context.getServiceImpl(IStaticFlowEntryPusherService.class);
    }

    @Deprecated
    public TopologyBase(ILinkDiscoveryService linkDiscoveryService, IStaticFlowEntryPusherService flowPusher, IHostHolderService hostHolder)
    {
        this.linkDiscoveryService = linkDiscoveryService;
        this.flowPusher = flowPusher;
        this.hostHolder = hostHolder;
        this.knownNodes = new HashMap<>();
        this.knownLinks = new HashMap<>();
    }

    public void updateTopoCache()
    {
        //flush all cache
        knownNodes.clear();
        knownLinks.clear();

        //
        if(ConfigParser.getInstance().DYNAMIC_GRAPH)
            LinkConfigParser.Update();

        //check known by floodlight switches
        Map<DatapathId, Set<Link>> switchLinks = linkDiscoveryService.getSwitchLinks();

        //add all known by controller switches first
        for (DatapathId id : switchLinks.keySet())
            knownNodes.put(id.toString(), new SwitchNodeBase(id.toString(), this));

        //add all known by controller switch-to-switch links
        for (Set<Link> links : switchLinks.values())
            for (Link l : links)
            {
                LinkInfo lInfo = linkDiscoveryService.getLinkInfo(l);
                if (lInfo.getLinkType() != ILinkDiscovery.LinkType.INVALID_LINK && (l.getDst() != null && l.getSrc() != null))
                {
                    Pair<INode, INode> linkKey1 = new ImmutablePair<>(knownNodes.get(l.getSrc().toString()), knownNodes.get(l.getDst().toString()));
                    Pair<INode, INode> linkKey2 = new ImmutablePair<>(knownNodes.get(l.getDst().toString()), knownNodes.get(l.getSrc().toString()));
                    if (!knownLinks.containsKey(linkKey1) && !knownLinks.containsKey(linkKey2))
                        knownLinks.put(linkKey1, new SwitchLinkBase(this, l));
                }
            }

        //add all known by controller hosts
        for (Map.Entry<Pair<IPv4Address, DatapathId>, Short> hostSwAndPort : hostHolder.getHostsAndSwitchesToPortsMap().entrySet())
        {
            String hostIP = hostSwAndPort.getKey().getLeft().toString();
            DatapathId switchDPID = hostSwAndPort.getKey().getRight();
            short port = hostSwAndPort.getValue();

            INode host = new HostNodeBase<>(hostIP, switchDPID.toString(), port, this);
            knownNodes.put(hostIP, host);
            Pair<INode, INode> key = new ImmutablePair<>(host, knownNodes.get(switchDPID.toString()));
            knownLinks.put(key, new HostLinkBase(this, hostIP, switchDPID, port));
        }

        //init neighbors based on cache!
        initNeighbors();

        if (log.isInfoEnabled())
            log.info("[NM-Topology] Topology Cache has been successfully updated!");
    }

    private void initNeighbors()
    {
        for (INode n : knownNodes.values())
            n.clearNeighbors();

        for (Map.Entry<Pair<INode, INode>, ILink> entry : knownLinks.entrySet())
        {
            entry.getKey().getLeft().addNeighbor(entry.getKey().getRight(), entry.getValue());
            entry.getKey().getRight().addNeighbor(entry.getKey().getLeft(), entry.getValue());
        }
    }

    @Override
    public void prepareTopoForDijkstra(double bw, boolean isSPF)
    {
        for (INode n : knownNodes.values())
        {
            n.setMinDist(Double.POSITIVE_INFINITY);
            n.setPredecessor(null);
            n.clearAllPredecessorsAndMinDists();
            n.clearDominantPaths();
        }

        for (ILink l : knownLinks.values())
            if (l.getBw() < bw)
                l.setAvCost(Double.POSITIVE_INFINITY);
            else if (isSPF)
                l.setAvCost(1);
            else
                l.setAvCost(l.getCost());
    }

    @Override
    public Collection<INode> getKnownNodes()
    {
        return knownNodes.values();
    }

    @Override
    public <T> INode<T> getNodeWithID(T id)
    {
        if (knownNodes.containsKey(id.toString()))
            return knownNodes.get(id.toString());
        else
        {
            IPv4Address nodeIP = null;
            if (id instanceof IPv4Address)
                nodeIP = (IPv4Address) id;
            else if (id instanceof String)
                nodeIP = IPv4Address.of((String) id);

            if (nodeIP != null && hostHolder.getSwitchesAndPortsByHost(nodeIP) != null)
            {
                Set<Pair<DatapathId, Short>> switchesAndPorts = hostHolder.getSwitchesAndPortsByHost(nodeIP);//todo here is an assumption that host is connected only to one border switch!!!
                if (switchesAndPorts != null && !switchesAndPorts.isEmpty())
                {
                    Pair<DatapathId, Short> switchAndPort = switchesAndPorts.iterator().next();

                    return new HostNodeBase<T>(id, (T) switchAndPort.getLeft().toString(),
                            switchAndPort.getRight(), this);
                }
            }
        }
        throw new UnsupportedOperationException("[NM-model]" + id.getClass() +
                " type of node ID currently isn't supported or "
                + id + " node is unknown for controller!");
    }

    public Map<Pair<INode,INode>, ILink> getKnownLinks()
    {
        return this.knownLinks;
    }

    public Set<Pair<IPv4Address, Short>> getSwitchHostsAndPorts(DatapathId dpid)
    {
        return hostHolder.getAllHostsAndPortsBySwitch(dpid);
    }

    public ILinkDiscoveryService getLinkDiscoveryService()
    {
        return context.getServiceImpl(ILinkDiscoveryService.class);
    }

    public IStaticFlowEntryPusherService getFlowPusher()
    {
        return context.getServiceImpl(IStaticFlowEntryPusherService.class);
    }
}
