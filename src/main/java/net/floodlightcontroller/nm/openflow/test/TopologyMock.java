package net.floodlightcontroller.nm.openflow.test;

import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;
import net.floodlightcontroller.nm.discovery.model.ITopology;

import org.apache.commons.lang3.tuple.Pair;
import org.projectfloodlight.openflow.types.IPv4Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;

/**
 * Created by chemo_000 on 2/10/2015.
 */
public class TopologyMock implements ITopology
{
    private Logger log = LoggerFactory.getLogger(TopologyMock.class);

    private INode<IPv4Address> n1; //h1
    private INode<IPv4Address> n2; //h2
    private INode<IPv4Address> n3; //h3
    private INode<IPv4Address> n4; //s1
    private INode<IPv4Address> n5; //s2
    private INode<IPv4Address> n6; //s3

    private IPv4Address ip1 = IPv4Address.of("10.0.0.1");
    private IPv4Address ip2 = IPv4Address.of("10.0.0.2");
    private IPv4Address ip3 = IPv4Address.of("10.0.0.3");
    private IPv4Address ip4 = IPv4Address.of("10.0.0.4");
    private IPv4Address ip5 = IPv4Address.of("10.0.0.5");
    private IPv4Address ip6 = IPv4Address.of("10.0.0.6");

    private ILink l14;
    private ILink l25;
    private ILink l36;
    private ILink l45;
    private ILink l46;
    private ILink l56;

    public TopologyMock()
    {
        double cost = Double.MAX_VALUE; //assume each link has infinite av bw

//mock links
        this.l14 = mock(ILink.class);
        stub(l14.getBw()).toReturn(cost);
        this.l45 = mock(ILink.class);
        stub(l45.getBw()).toReturn(cost);
        this.l25 = mock(ILink.class);
        stub(l25.getBw()).toReturn(cost);
        this.l56 = mock(ILink.class);
        stub(l56.getBw()).toReturn(cost);
        this.l36 = mock(ILink.class);
        stub(l36.getBw()).toReturn(cost);
        this.l46 = mock(ILink.class);
        stub(l46.getBw()).toReturn(5.0);

//mock nodes
        this.n1 = mock(INode.class);
        stub(n1.getID()).toReturn(ip1);
        stub(n1.toString()).toReturn("h1");
        this.n2 = mock(INode.class);
        stub(n2.getID()).toReturn(ip2);
        stub(n2.toString()).toReturn("h2");
        this.n3 = mock(INode.class);
        stub(n3.getID()).toReturn(ip3);
        stub(n3.toString()).toReturn("h3");
        this.n4 = mock(INode.class);
        stub(n4.getID()).toReturn(ip4);
        stub(n4.toString()).toReturn("s1");
        this.n5 = mock(INode.class);
        stub(n5.getID()).toReturn(ip5);
        stub(n5.toString()).toReturn("s2");
        this.n6 = mock(INode.class);
        stub(n6.getID()).toReturn(ip6);
        stub(n6.toString()).toReturn("s3");

//mock ports (0 port for host)
        //stub host-to-switch ports
        stub(l14.getSrcPort(n1, n4)).toReturn((short) 0);
        stub(l14.getSrcPort(n4, n1)).toReturn((short) 1);
        stub(l25.getSrcPort(n2, n5)).toReturn((short) 0);
        stub(l25.getSrcPort(n5, n2)).toReturn((short) 1);
        stub(l36.getSrcPort(n3, n6)).toReturn((short) 0);
        stub(l36.getSrcPort(n6, n3)).toReturn((short) 1);

        //stub switch-to-switch ports
        stub(l45.getSrcPort(n4, n5)).toReturn((short) 2);
        stub(l45.getSrcPort(n5, n4)).toReturn((short) 2);
        stub(l46.getSrcPort(n4, n6)).toReturn((short) 3);
        stub(l46.getSrcPort(n6, n4)).toReturn((short) 2);
        stub(l56.getSrcPort(n5, n6)).toReturn((short) 3);
        stub(l56.getSrcPort(n6, n5)).toReturn((short) 3);

//create neighbors for each node and stub them for mocked nodes
        Map<INode<IPv4Address>, ILink> neighbors1 = new HashMap<INode<IPv4Address>, ILink>()
        {
            {
                put(n4, l14);
            }
        };
        Map<INode<IPv4Address>, ILink> neighbors2 = new HashMap<INode<IPv4Address>, ILink>()
        {
            {
                put(n5, l25);
            }
        };
        Map<INode<IPv4Address>, ILink> neighbors3 = new HashMap<INode<IPv4Address>, ILink>()
        {
            {
                put(n6, l36);
            }
        };
        Map<INode<IPv4Address>, ILink> neighbors4 = new HashMap<INode<IPv4Address>, ILink>()
        {
            {
                put(n1, l14);
                put(n5, l45);
                put(n6, l46);
            }
        };
        Map<INode<IPv4Address>, ILink> neighbors5 = new HashMap<INode<IPv4Address>, ILink>()
        {
            {
                put(n2, l25);
                put(n4, l45);
                put(n6, l56);
            }
        };
        Map<INode<IPv4Address>, ILink> neighbors6 = new HashMap<INode<IPv4Address>, ILink>()
        {
            {
                put(n3, l36);
                put(n5, l56);
                put(n4, l46);
            }
        };

        stub(n1.getNeighbors()).toReturn(neighbors1);
        stub(n2.getNeighbors()).toReturn(neighbors2);
        stub(n3.getNeighbors()).toReturn(neighbors3);
        stub(n4.getNeighbors()).toReturn(neighbors4);
        stub(n5.getNeighbors()).toReturn(neighbors5);
        stub(n6.getNeighbors()).toReturn(neighbors6);
    }

    @Override
    public <T> INode<T> getNodeWithID(T id)
    {
        if (log.isInfoEnabled())
            log.info("[TEST-Topology] Node with ID:" + id + " has been requested!");

        if (id.equals(ip1))
            return (INode<T>) n1;
        else if (id.equals(ip2))
            return (INode<T>) n2;
        else if (id.equals(ip3))
            return (INode<T>) n3;
        else if (id.equals(ip4))
            return (INode<T>) n4;
        else if (id.equals(ip5))
            return (INode<T>) n5;
        else if (id.equals(ip6))
            return (INode<T>) n6;
        else
            return null;
    }

    @Override
    public void setContext(FloodlightModuleContext context)
    {

    }

    @Override
    public void updateTopoCache()
    {

    }

    @Override
    public void prepareTopoForDijkstra(double bw, boolean isSPF)
    {

    }

    @Override
    public Collection<INode> getKnownNodes()
    {
        return null;
    }

    @Override
    public Map<Pair<INode, INode>, ILink> getKnownLinks()
    {
        return null;
    }
}
