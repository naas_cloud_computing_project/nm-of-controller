package net.floodlightcontroller.nm.openflow.customservices;

import net.floodlightcontroller.core.module.IFloodlightService;

/**
 * Created by chemo_000 on 2/10/2015.
 */
public interface IVLRequestHandlerService extends Runnable, IFloodlightService
{
}
