package net.floodlightcontroller.nm.openflow.customservices;

import net.floodlightcontroller.core.module.IFloodlightService;
import net.floodlightcontroller.nm.discovery.model.INode;
import net.floodlightcontroller.nm.discovery.model.ITopology;

import java.util.List;
import java.util.Map;

/**
 * Created by chemo_000 on 2/10/2015.
 */
public interface IRouteFinderService extends IFloodlightService //todo java docs
{
    public enum Type{NM, NMGEN, ED, IBF, EBFS}

    public <T> Map<List<INode<T>>, Double> findRoutesAndBw(T srcID, T dstID, boolean isSplittable, double bw);

    public <T> Map<List<INode<T>>, Double> findRoutesAndBwCost(T srcID, T dstID, boolean isSplittable, double bw, double cost);

    public ITopology getTopo();
}
