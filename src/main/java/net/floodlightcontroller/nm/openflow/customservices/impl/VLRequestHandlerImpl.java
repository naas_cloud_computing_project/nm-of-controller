package net.floodlightcontroller.nm.openflow.customservices.impl;

import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.nm.openflow.customservices.IRouteFinderService;
import net.floodlightcontroller.nm.openflow.customservices.IVLRequestHandlerService;
import net.floodlightcontroller.nm.allocation.RouteBroker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

/**
 * Created by chemo_000 on 2/10/2015.
 */
public class VLRequestHandlerImpl implements IVLRequestHandlerService
{
    private static Logger log = LoggerFactory.getLogger(VLRequestHandlerImpl.class);

    private IRouteFinderService iRouteFinderService;
    private IStaticFlowEntryPusherService flowEntryPusher;
    private FloodlightModuleContext context;

    public VLRequestHandlerImpl(FloodlightModuleContext context)
    {
        this.context = context;
    }

    @Deprecated
    public VLRequestHandlerImpl(IRouteFinderService iRouteFinderService, IStaticFlowEntryPusherService flowEntryPusher)
    {
        this.iRouteFinderService = iRouteFinderService;
        this.flowEntryPusher = flowEntryPusher;

    }

    @Override
    public void run()
    {
        try
        {
            ServerSocket serverSocket = new ServerSocket(42015);// socket is hardcoded, don't change it.
            log.info("[NM-services]NM VL request server has been started on port=42015");
            while (true)
            {
                Socket s = serverSocket.accept();// open connection

                if (context != null)
                    initServices();
                else
                    RouteBroker.getInstance().setFlowPusherService(flowEntryPusher);

                BufferedReader ins = new BufferedReader(new InputStreamReader(s.getInputStream()));
                PrintStream outs = new PrintStream(s.getOutputStream());

                boolean repeat = true;
                try
                {
                    while (repeat)
                    {
                        String src_ip;
                        String dst_ip;
                        double reqBw;
                        boolean isSplittable;

                        try
                        {
                            outs.println("Input src IP:");
                            src_ip = ins.readLine();

                            outs.println("Input dst IP:");
                            dst_ip = ins.readLine();

                            outs.println("Input min BW in Mbps:");
                            reqBw = Double.valueOf(ins.readLine());

                            outs.println("Is path splittable?");
                            isSplittable = Boolean.valueOf(ins.readLine());

                            log.info("[NM-services]NM Client specified next VL request: src=" + src_ip
                                    + " dst=" + dst_ip
                                    + " bw=" + reqBw
                                    + " splittable=" + isSplittable);

//                           results
//                            VirtualLink vl;
//                            Set<VirtualLink> vls = new HashSet<>();
//                            try
//                            {
                            Map res = iRouteFinderService.findRoutesAndBw(src_ip, dst_ip, isSplittable, reqBw);
                            outs.println("Do you want see result?");
                            if (Boolean.valueOf(ins.readLine()))
                            {
                                outs.println("[NM-services]Suitable rotes and bw: " + res);
                                outs.println("Associated with routes circuits: " + flowEntryPusher.getFlows());
                            }

//                                vl = new VirtualLink(src_ip, dst_ip, reqBw, VirtualLink.Status.ALLOCATED);
//                            } catch (Exception e)
//                            {
//                                vl = new VirtualLink(src_ip, dst_ip, reqBw, VirtualLink.Status.FAILED);
//                            }
//                            vls.add(vl);
//                            VLHolder.getInstance().addUserVL("user1", vls);

                            outs.println("Delete existing flows?");
                            boolean delete = Boolean.valueOf(ins.readLine());
                            if (delete)
                            {
                                flowEntryPusher.deleteAllFlows();
                                outs.println("[NM-Resources]Current Flows are: " + flowEntryPusher.getFlows());
                            }

                        } catch (Exception ex)
                        {
                            log.error("[NM-services]next exception occurred: ", ex);
                            outs.println("ERROR");
                            outs.println(ex.toString());
                        }

                        outs.println("Repeat?");
                        repeat = Boolean.valueOf(ins.readLine());
                    }
                } catch (Exception e)
                {
                    log.error("[NM-services]next exception occurred: ", e);
                }
            }
        } catch (IOException ioe)
        {
            log.error("[NM-services]next IO Exception occurred: ", ioe);
        }
    }

    private void initServices()
    {
        flowEntryPusher = context.getServiceImpl(IStaticFlowEntryPusherService.class);
        if (RouteBroker.getInstance().needFlowPusher())
            RouteBroker.getInstance().setFlowPusherService(flowEntryPusher);

        iRouteFinderService = context.getServiceImpl(IRouteFinderService.class);
    }
}
