package net.floodlightcontroller.nm.openflow.customservices.impl;

import net.floodlightcontroller.nm.web.model.VirtualLink;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by chemo_000 on 4/27/2015.
 */
public class VLHolder
{
    private static VLHolder instance = new VLHolder();

    private Map<String, Set<VirtualLink>> userVL;

    public static VLHolder getInstance()
    {
        return instance;
    }

    private VLHolder()
    {
        userVL = new ConcurrentHashMap<>();
    }

    public Set<VirtualLink> getUserVL(String userName)
    {
        return userVL.get(userName);
    }

    public void addUserVL(String userName, Set<VirtualLink> vls)
    {
        if(userVL.containsKey(userName))

            userVL.get(userName).addAll(vls);
        else
            userVL.put(userName, vls);
    }

    public void removeAllVLs()
    {
        userVL.clear();
    }
}
