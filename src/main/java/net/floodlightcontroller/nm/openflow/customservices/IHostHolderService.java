package net.floodlightcontroller.nm.openflow.customservices;

import org.apache.commons.lang3.tuple.Pair;
import org.projectfloodlight.openflow.types.DatapathId;
import org.projectfloodlight.openflow.types.IPv4Address;

import java.util.Map;
import java.util.Set;

/**
 * Created by chemo_000 on 2/15/2015.
 */
public interface IHostHolderService //todo java docs
{
    public boolean containsIPAndDPID(IPv4Address ip, DatapathId dpid);

    public void put(Pair<IPv4Address, DatapathId> k, Short v);

    public Set<Pair<DatapathId, Short>> getSwitchesAndPortsByHost(IPv4Address hostIP);

    public Set<Pair<IPv4Address, Short>> getAllHostsAndPortsBySwitch(DatapathId dpid);

    public Map<Pair<IPv4Address, DatapathId>, Short> getHostsAndSwitchesToPortsMap();
}
