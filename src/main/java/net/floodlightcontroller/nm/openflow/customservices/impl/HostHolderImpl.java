package net.floodlightcontroller.nm.openflow.customservices.impl;

import net.floodlightcontroller.nm.openflow.customservices.IHostHolderService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.projectfloodlight.openflow.types.DatapathId;
import org.projectfloodlight.openflow.types.IPv4Address;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by chemo_000 on 2/15/2015.
 */
public class HostHolderImpl implements IHostHolderService
{
    private static Logger log = LoggerFactory.getLogger(HostHolderImpl.class);

    private Map<Pair<IPv4Address, DatapathId>, Short> hostsAndSwitchesToPorts;

    private static HostHolderImpl instance = new HostHolderImpl();

    public static HostHolderImpl getInstance()
    {
        return instance;
    }

    private HostHolderImpl()
    {
        hostsAndSwitchesToPorts = new ConcurrentHashMap<>();
    }


    @Override
    public synchronized boolean containsIPAndDPID(IPv4Address ip, DatapathId dpid)//todo add due dates for entries
    {
        Pair<IPv4Address, DatapathId> k = new ImmutablePair<>(ip, dpid);
        return hostsAndSwitchesToPorts.containsKey(k);
    }

    @Override
    public synchronized void put(Pair<IPv4Address, DatapathId> k, Short v)
    {
        if (hostsAndSwitchesToPorts.containsKey(k))
            hostsAndSwitchesToPorts.remove(k);

        hostsAndSwitchesToPorts.put(k, v);

        if (log.isTraceEnabled())
            log.trace("[NM-OFService]Next IP=" + k
                    + " was added. All known hosts are: " + hostsAndSwitchesToPorts.keySet());
    }

    @Override
    public Set<Pair<DatapathId, Short>> getSwitchesAndPortsByHost(IPv4Address hostIP)
    {
        Set<Pair<DatapathId, Short>> switchesAndPorts = new HashSet<>();

        for (Map.Entry<Pair<IPv4Address, DatapathId>, Short> hostToSwithAndPort : hostsAndSwitchesToPorts.entrySet())
            if (hostToSwithAndPort.getKey().getLeft().equals(hostIP))
            {
                DatapathId k = hostToSwithAndPort.getKey().getRight();
                Short v = hostToSwithAndPort.getValue();

                switchesAndPorts.add(new ImmutablePair<>(k, v));
            }

        return switchesAndPorts;
    }

    @Override
    public Set<Pair<IPv4Address, Short>> getAllHostsAndPortsBySwitch(DatapathId dpid)
    {
        Set<Pair<IPv4Address, Short>> hostsAndPorts = new HashSet<>();

        for (Map.Entry<Pair<IPv4Address, DatapathId>, Short> hostToSwithAndPort : hostsAndSwitchesToPorts.entrySet())
            if (hostToSwithAndPort.getKey().getRight().equals(dpid))
            {
                IPv4Address k = hostToSwithAndPort.getKey().getLeft();
                Short v = hostToSwithAndPort.getValue();

                hostsAndPorts.add(new ImmutablePair<>(k, v));
            }

        return hostsAndPorts;
    }

    @Override
    public Map<Pair<IPv4Address, DatapathId>, Short> getHostsAndSwitchesToPortsMap()
    {
        return hostsAndSwitchesToPorts;
    }
}
