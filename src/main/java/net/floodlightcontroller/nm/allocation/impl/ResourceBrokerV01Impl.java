package net.floodlightcontroller.nm.allocation.impl;

import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.util.MatchUtils;
import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;
import net.floodlightcontroller.nm.allocation.IResourceBroker;
import org.projectfloodlight.openflow.protocol.*;
import org.projectfloodlight.openflow.protocol.action.OFAction;
import org.projectfloodlight.openflow.protocol.action.OFActionEnqueue;
import org.projectfloodlight.openflow.protocol.match.Match;
import org.projectfloodlight.openflow.types.DatapathId;
import org.projectfloodlight.openflow.types.IPv4Address;
import org.projectfloodlight.openflow.types.OFPort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by chemo_000 on 2/11/2015.
 */
public class ResourceBrokerV01Impl implements IResourceBroker //todo refactor, java docs
{
    private Logger log = LoggerFactory.getLogger(ResourceBrokerV01Impl.class);
    private IStaticFlowEntryPusherService flowPusher;

    static OFFactory factory;

    public ResourceBrokerV01Impl()
    {
        factory = OFFactories.getFactory(OFVersion.OF_10);
    }

    //circuit rate limiting isn't supported
    @Override
    public <T> void allocateSpecifiedBandwidth(INode<T> src, INode<T> dst, INode<T> current, INode<T> next, ILink link, double bw)
    {
        System.out.println("Flow Pusher = " + flowPusher);

        if (flowPusher != null && !src.equals(current))
        {
            long queueId = bwToQueueId(bw);
            if (log.isDebugEnabled())
                log.debug("[NM-Resources]Allocation of path from " + src.toString()
                        + " to " + dst.toString()
                        + " on node " + current.toString()
                        + " desired queue=" + queueId
                        + " has been started!");

            allocateCircuit(src, dst, current, next, link, queueId); //allocate circuit for forward path
        }
    }

    @Override
    public <T> void releaseSpecifiedBandwidth(INode<T> src, INode<T> dst, INode<T> current, INode<T> next, ILink link, double bw)
    {
        if (flowPusher != null && !src.equals(current))
        {
            if (log.isDebugEnabled())
                log.debug("[NM-Resources]Removing of path from " + src.toString()
                        + " to " + dst.toString()
                        + " on node " + current.toString()
                        + " has been started!");

            deleteCircuit(src, dst, current, next, link);
        }
    }

    @Override
    public void setFlowPusher(IStaticFlowEntryPusherService flowPusher)
    {
        if (log.isTraceEnabled())
            log.trace("[NM-Resources]Next Flow Pusher has been set: " + flowPusher);

        this.flowPusher = flowPusher;
    }

    @Override
    public boolean needFlowPusher()
    {
        return this.flowPusher==null;
    }

    //currently support only IPv4
    protected <T> void allocateCircuit(INode<T> src, INode<T> dst, INode<T> current, INode<T> next, ILink link, long queueId)
    {
        if (src.getID() instanceof String)
        {
            //set up IP circuit
            IPv4Address srcIP = IPv4Address.of((String) src.getID());
            IPv4Address dstIP = IPv4Address.of((String) dst.getID());
            DatapathId currentDPID = DatapathId.of((String) current.getID());
            OFPort outPort = OFPort.of(getPort(current, next, link));

            //determine flows names
            //ip packet
            String ipName = "IP-from-" + srcIP.toString()
                    + "-to-" + dstIP.toString()
                    + "-at-" + currentDPID.toString()
                    + "-time-" + System.currentTimeMillis();

            //arp packet
            String arpName = "ARP-from-" + srcIP.toString()
                    + "-to-" + dstIP.toString()
                    + "-at-" + currentDPID.toString()
                    + "-time-" + System.currentTimeMillis();

            //find previous IP and ARP flows
            Map<String, OFFlowMod> oldFlows = flowPusher.getFlows(currentDPID);
            List<OFAction> oldActionIP = new ArrayList<>();
            List<OFAction> oldActionARP = new ArrayList<>();

            if (oldFlows != null && !oldFlows.isEmpty())
            {
                OFFlowMod existIPFlow = oldFlows.get(ipName);//previous ip flow
                //delete previous flow but save actions to support multipath!!!
                if (existIPFlow != null)
                {
                    oldActionIP.addAll(existIPFlow.getActions());
                    System.out.println("Next flow was discovered: " + existIPFlow);
                    flowPusher.deleteFlow(ipName);
                }

                OFFlowMod existARPFlow = oldFlows.get(arpName);//previous arp flow
                //delete previous flow but save actions to support multipath!!!
                if (existIPFlow != null)
                {
                    oldActionARP.addAll(existARPFlow.getActions());
                    flowPusher.deleteFlow(arpName);
                }
            }

            // Declare the actions
            List<OFAction> actionIP = new ArrayList<>();
            long oldQueue = getPortQueueForIP(oldActionIP, outPort); //check if already have a queue for the <flow,port>

            if (oldQueue == 0)//means we don't already have a queue on specified port: @outPort
                actionIP.addAll(oldActionIP);//save previous action for the flow [multipath situation]
//            OFAction newIPAction = factory.actions().output(outPort, Integer.MAX_VALUE);
            OFAction newIPAction = factory.actions().enqueue(outPort, queueId + oldQueue);
//            if (!actionIP.contains(newIPAction))
            actionIP.add(newIPAction);

            // Declare the flow
            OFFlowMod fmIP = factory.buildFlowModify().build();

            // Declare the match
            Match mIP = MatchUtils.fromString("eth_type=0x800,ipv4_src=" + srcIP.toString() + ",ipv4_dst=" + dstIP.toString(), //
                    factory.getVersion());
            fmIP = fmIP.createBuilder().setMatch(mIP)
                    .setActions(actionIP)
                    .setIdleTimeout(FLOW_IDLE_TIMEOUT)
                    .setHardTimeout(FLOW_HARD_TIMEOUT)
                    .build();

            // Push the flow
            //log.info("[NM-Resources]New IP circuit path name was created: " + ipName);
            flowPusher.addFlow(ipName, fmIP, currentDPID);
            log.info("[NM-Resources]New IP circuit path was allocated: " + ipName);

            //set up ARP circuit
            // Declare the actions
            List<OFAction> actionArp = new ArrayList<>();
            actionArp.addAll(oldActionARP);
            OFAction newARPAction = factory.actions().output(outPort, Integer.MAX_VALUE);
            if (!actionArp.contains(newARPAction))
                actionArp.add(newARPAction);

            // Declare the flow
            OFFlowMod fmArp = factory.buildFlowModify().build();
            // Declare the match
            Match mArp = MatchUtils.fromString("eth_type=0x806,ipv4_src=" + srcIP.toString() + ",ipv4_dst=" + dstIP.toString(),
                    factory.getVersion());
            fmArp = fmArp.createBuilder().setMatch(mArp)
                    .setActions(actionArp)
                    .setIdleTimeout(FLOW_IDLE_TIMEOUT)
                    .setHardTimeout(FLOW_HARD_TIMEOUT)
                    .build();

            // Push the flow
//            System.out.println("[NM-Resources]New IP circuit path name was created: " + ipName);
            flowPusher.addFlow(arpName, fmArp, DatapathId.of((String) current.getID()));
            log.info("[NM-Resources]New ARP circuit path was allocated: " + ipName);
        }
    }

    protected <T> void deleteCircuit(INode<T> src, INode<T> dst, INode<T> current, INode<T> next, ILink link)
    {
        if (src.getID() instanceof IPv4Address)
        {
            String ipName = "IP-from-" + src.getID().toString()
                    + "-to-" + dst.getID().toString()
                    + "-at-" + current.getID().toString()
                    + "-next-" + next.getID().toString();

            flowPusher.deleteFlow(ipName);
            String arpName = "ARP-from-" + src.getID().toString()
                    + "-to-" + dst.getID().toString()
                    + "-at-" + current.getID().toString()
                    + "-next-" + next.getID().toString();
            flowPusher.deleteFlow(arpName);
        }
    }

    private long bwToQueueId(double bw)
    {
        boolean dividedByFive = bw % BW_TO_QUEUE_ID_RATIO == 0;
        long queueID = (long) (bw / BW_TO_QUEUE_ID_RATIO);

        if (!dividedByFive)
            queueID++;

        return queueID;
    }

    //here we assume that we always have only one enqueue action per port for particular flow mode [only IP data]
    private long getPortQueueForIP(List<OFAction> oldActions, OFPort outPort)
    {
        //if(oldActions!= null && !oldActions.isEmpty()) //redundant check!
        for (OFAction action : oldActions)
        {
            if (action.getType().equals(OFActionType.ENQUEUE))
            {
                OFActionEnqueue enqueue = (OFActionEnqueue) action;
                if (enqueue.getPort().equals(outPort))
                    return enqueue.getQueueId();
            }
        }

        return 0;
    }

    private <T> short getPort(INode<T> current, INode<T> next, ILink link)
    {
        return link.getSrcPort(current, next);
    }
}
