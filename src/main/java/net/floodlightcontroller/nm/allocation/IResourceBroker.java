package net.floodlightcontroller.nm.allocation;

import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.nm.NMConstants;
import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;

/**
 * Created by chemo_000 on 2/6/2015.
 */
public interface IResourceBroker extends NMConstants
{
    public <T> void allocateSpecifiedBandwidth(INode<T> src, INode<T> dst, INode<T> current, INode<T> next, ILink link, double cost);

    public <T> void releaseSpecifiedBandwidth(INode<T> src, INode<T> dst, INode<T> current, INode<T> next, ILink link, double cost);

    public void setFlowPusher(IStaticFlowEntryPusherService flowPusher);

    public boolean needFlowPusher();
}
