package net.floodlightcontroller.nm.utils;

import net.floodlightcontroller.nm.xml.ConfigParser;

import java.io.FileWriter;
import java.util.List;
import java.util.Map;

/**
 * Created by chemo_000 on 9/2/2016.
 */
public class LogWriter
{
    private String filename;

    private String buffer;

    private static LogWriter instance = new LogWriter("exported_data.csv");

    public static LogWriter getInstance()
    {
        return instance;
    }

    private LogWriter(String filename)
    {
        this.filename = filename;
        this.buffer = "";
        createLogFile();
    }

    private void createLogFile()
    {
        try
        {
            FileWriter writer = new FileWriter(filename);
            writer.append("UsedSW, Bw, Path Length, VL Mapping, VL Alloc, VL Embedding\n");
            writer.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void logInfoToFile(String s)
    {
        try
        {
            FileWriter writer = new FileWriter(filename, true);
            //first write buffer
            writer.append(flushBuffer());
            //then append current log
            writer.append(s);
            writer.close();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void logInfoToBuffer(String s)
    {
        this.buffer += s;
    }

    private String flushBuffer()
    {
        String tempBuffer = this.buffer;
        this.buffer = "";
        return tempBuffer;
    }
}
