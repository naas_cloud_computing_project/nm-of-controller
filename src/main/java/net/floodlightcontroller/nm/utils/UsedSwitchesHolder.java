package net.floodlightcontroller.nm.utils;

import net.floodlightcontroller.nm.discovery.model.INode;

import java.io.FileWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by chemo_000 on 9/2/2016.
 */
public class UsedSwitchesHolder
{
    private Set usedSwitches;

    private static UsedSwitchesHolder instance = new UsedSwitchesHolder();

    public static UsedSwitchesHolder getInstance()
    {
        return instance;
    }

    private UsedSwitchesHolder()
    {
        this.usedSwitches = new HashSet<>();
    }

    public void addUsedSwitches(List path)
    {
        //do not include src and dest as they are hosts
       if (path.size()>2)
       {
           usedSwitches.addAll(path.subList(1, path.size()-1));
       }
    }

    public int getNumOfUsedSwitches()
    {
        return this.usedSwitches.size();
    }
}
