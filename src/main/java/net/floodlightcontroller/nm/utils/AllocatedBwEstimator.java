package net.floodlightcontroller.nm.utils;

import net.floodlightcontroller.staticflowentry.IStaticFlowEntryPusherService;
import net.floodlightcontroller.nm.NMConstants;
import org.projectfloodlight.openflow.protocol.OFActionType;
import org.projectfloodlight.openflow.protocol.OFFlowMod;
import org.projectfloodlight.openflow.protocol.action.OFAction;
import org.projectfloodlight.openflow.protocol.action.OFActionEnqueue;
import org.projectfloodlight.openflow.types.DatapathId;

import java.util.Map;

/**
 * Created by chemo_000 on 4/29/2015.
 */
public class AllocatedBwEstimator implements NMConstants
{
    /**
     *
     * @param dpid - dpid of src switch
     * @param port - src port
     * @param flowPusher - IStaticFlowPusherService of Floodlight
     * @return - allocated bw on @dpid switch and specified @port
     */
    public double estimateAllocatedBw(DatapathId dpid, short port, IStaticFlowEntryPusherService flowPusher)
    {
        double allocatedCost = 0;

        Map<String, OFFlowMod> flows = flowPusher.getFlows(dpid);

        if (flows != null)
            for (OFFlowMod flow : flows.values())
                for (OFAction action : flow.getActions())
                    if (action.getType().equals(OFActionType.ENQUEUE))
                    {
                        OFActionEnqueue queue = (OFActionEnqueue) action;

                        if (queue.getPort().getShortPortNumber() == port)
                            //determine cost by queue id and queue id/cost ratio
                            allocatedCost += queue.getQueueId() * BW_TO_QUEUE_ID_RATIO;
                    }

        return allocatedCost;
    }
}
