/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.floodlightcontroller.nm.utils;

import net.floodlightcontroller.nm.discovery.model.INode;

import java.util.*;

/**
 * @author dm190
 */
public class CartesianUtils
{

    public CartesianUtils()
    {
    }

    public <T> Set<List<INode<T>>> cartesianProduct(final List<Set<INode<T>>> nodes)
    {
        return createCartesianProduct(nodes);
    }

    public <T> Set<List<INode<T>>> restrictedCartesianProduct(final List<Set<INode<T>>> nodes, double cost)
    {
        return createRestrictedCartesianProduct(nodes, cost);
    }

    private <T> Set<List<INode<T>>> createCartesianProduct(final List<Set<INode<T>>> nodes)
    {
        switch (nodes.size())
        {
            case 0:
                return new HashSet<>();
            case 1:
                return new HashSet<List<INode<T>>>()
                {
                    {
                        add(Collections.singletonList(nodes.get(0).iterator().next()));
                    }
                };
            default:
                return createCartesianProductInternal(0, nodes);
        }
    }

    private <T> Set<List<INode<T>>> createRestrictedCartesianProduct(final List<Set<INode<T>>> nodes, double bw)
    {
        switch (nodes.size())
        {
            case 0:
                return new HashSet<>();
            case 1:
                return new HashSet<List<INode<T>>>()
                {
                    {
                        add(Collections.singletonList(nodes.get(0).iterator().next()));
                    }
                };
            default:
                return createRestrictedCartesianProductInt(0, nodes, bw);
        }
    }

    private <T> Set<List<INode<T>>> createCartesianProductInternal(int index, final List<Set<INode<T>>> nodes)
    {
        Set<List<INode<T>>> ret = new HashSet<>();

        if (index == nodes.size())
            ret.add(new ArrayList<INode<T>>());
        else
        {
            Set<List<INode<T>>> q = createCartesianProductInternal(index + 1, nodes);

            for (final INode obj : nodes.get(index))
                for (final List<INode<T>> list : q)
                {
// logic with restriction of routes total number
//                    if (routesLimit >= 0 && ret.size() >= routesLimit)
//                    {
//                        break;
//                    }

                    //check that previous node can connect new one
                    //@todo as an option property to exclude loops: || list.contains(obj))

                    if ((!list.isEmpty() && !obj.getNeighbors().keySet().contains(list.get(0))) || list.contains(obj))
                        continue;

                    ret.add(new ArrayList<INode<T>>()
                    {
                        {
                            add(obj);
                            addAll(list);
                        }
                    });
                }
        }
        return ret;
    }

    private <T> Set<List<INode<T>>> createRestrictedCartesianProductInt(int index, final List<Set<INode<T>>> nodes, double bw)
    {
        Set<List<INode<T>>> ret = new HashSet<>();
        if (index == nodes.size() - 1)
            ret.add(new ArrayList<INode<T>>()
            {
                {
                    addAll(nodes.get(nodes.size() - 1));
                }
            });
        else
        {
            Set<List<INode<T>>> q = createRestrictedCartesianProductInt(index + 1, nodes, bw);
            for (final INode<T> obj : nodes.get(index))
                for (final List<INode<T>> list : q)
                    if (obj.getNeighbors().keySet().contains(list.get(0)) &&
                            obj.getNeighbors().get(list.get(0)).getBw() >= bw)
                    {
                        ret.add(new ArrayList<INode<T>>()
                        {
                            {
                                add(obj);
                                addAll(list);
                            }
                        });
                        break;
                    }
        }
        return ret;
    }
}
