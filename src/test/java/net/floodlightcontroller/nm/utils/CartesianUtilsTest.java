package net.floodlightcontroller.nm.utils;

import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;
import net.floodlightcontroller.nm.discovery.model.ITopology;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;

/**
 * Created by chemo_000 on 2/6/2015.
 */
public class CartesianUtilsTest
{
    private INode<Integer> n1;
    private INode<Integer> n2;
    private INode<Integer> n3;
    private INode<Integer> n4;
    private INode<Integer> n5;

    private ILink l12;
    private ILink l13;
    private ILink l24;
    private ILink l35;
    private ILink l54;

    private ITopology t;

    private List<Set<INode<Integer>>> neighborhood1;
    private Set<INode<Integer>> neighborhood2;

    private List<INode<Integer>> route1;
    private List<INode<Integer>> route2;

    @Before
    public void setUp()
    {
        this.t = mock(ITopology.class);

        this.l12 = mock(ILink.class);
        stub(l12.getBw()).toReturn(3.0);
        this.l13 = mock(ILink.class);
        stub(l13.getBw()).toReturn(4.0);
        this.l24 = mock(ILink.class);
        stub(l24.getBw()).toReturn(6.0);
        this.l35 = mock(ILink.class);
        stub(l35.getBw()).toReturn(8.0);
        this.l54 = mock(ILink.class);
        stub(l54.getBw()).toReturn(9.0);

        this.n1 = mock(INode.class);
        stub(n1.getID()).toReturn(1);
        stub(t.getNodeWithID(1)).toReturn(n1);
        this.n2 = mock(INode.class);
        stub(n2.getID()).toReturn(2);
        stub(t.getNodeWithID(2)).toReturn(n2);
        this.n3 = mock(INode.class);
        stub(n3.getID()).toReturn(3);
        stub(t.getNodeWithID(3)).toReturn(n3);
        this.n4 = mock(INode.class);
        stub(n4.getID()).toReturn(4);
        stub(t.getNodeWithID(4)).toReturn(n4);
        this.n5 = mock(INode.class);
        stub(n5.getID()).toReturn(5);
        stub(t.getNodeWithID(5)).toReturn(n5);

        Map<INode<Integer>, ILink> neighbors1 = mock(Map.class);
        stub(neighbors1.keySet()).toReturn(new HashSet<INode<Integer>>()
        {{
                add(n2);
                add(n3);
            }});
        stub(neighbors1.get(n2)).toReturn(l12);
        stub(neighbors1.get(n3)).toReturn(l13);

        Map<INode<Integer>, ILink> neighbors2 = mock(Map.class);
        stub(neighbors2.keySet()).toReturn(new HashSet<INode<Integer>>()
        {{
                add(n1);
                add(n4);
            }});
        stub(neighbors2.get(n1)).toReturn(l12);
        stub(neighbors2.get(n4)).toReturn(l24);

        Map<INode<Integer>, ILink> neighbors3 = mock(Map.class);
        stub(neighbors3.keySet()).toReturn(new HashSet<INode<Integer>>()
        {{
                add(n1);
                add(n5);
            }});
        stub(neighbors3.get(n1)).toReturn(l13);
        stub(neighbors3.get(n5)).toReturn(l35);

        Map<INode<Integer>, ILink> neighbors4 = mock(Map.class);
        stub(neighbors4.keySet()).toReturn(new HashSet<INode<Integer>>()
        {{
                add(n2);
                add(n5);
            }});
        stub(neighbors4.get(n2)).toReturn(l24);
        stub(neighbors4.get(n5)).toReturn(l54);

        Map<INode<Integer>, ILink> neighbors5 = mock(Map.class);
        stub(neighbors5.keySet()).toReturn(new HashSet<INode<Integer>>()
        {{
                add(n4);
                add(n3);
            }});
        stub(neighbors5.get(n3)).toReturn(l35);
        stub(neighbors5.get(n4)).toReturn(l54);

        stub(n1.getNeighbors()).toReturn(neighbors1);
        stub(n2.getNeighbors()).toReturn(neighbors2);
        stub(n3.getNeighbors()).toReturn(neighbors3);
        stub(n4.getNeighbors()).toReturn(neighbors4);
        stub(n5.getNeighbors()).toReturn(neighbors5);

        neighborhood1 = new ArrayList<Set<INode<Integer>>>()
        {{
                add(Collections.singleton(n1));
                add(new HashSet<INode<Integer>>()
                {{
                        add(n2);
                        add(n3);
                    }});
                add(new HashSet<INode<Integer>>()
                {{
                        add(n1);
                        add(n4);
                        add(n5);
                    }});
            }};

        neighborhood2 = new HashSet<INode<Integer>>()
        {{
                add(n2);
                add(n3);
                add(n4);
                add(n5);
            }};

        route1 = new LinkedList<INode<Integer>>()
        {
            {
                add(n1);
                add(n2);
                add(n4);
            }
        };

        route2 = new LinkedList<INode<Integer>>()
        {
            {
                add(n1);
                add(n3);
                add(n5);
                add(n4);
            }
        };
    }

    @Test
    public void cartesianProduct_shouldReturnCartesianProductOrRoutes_butOnlyForConnectedNodes()
    {
        CartesianUtils cu = new CartesianUtils();
        Set<INode<Integer>> lastNeighborhood = neighborhood1.set(neighborhood1.size()-1, Collections.singleton(n4));
        Set<List<INode<Integer>>> result = cu.cartesianProduct(neighborhood1);

        assertEquals(1, result.size());
        assertEquals(route1, result.iterator().next());

        neighborhood1.set(neighborhood1.size()-1, lastNeighborhood);
        neighborhood1.add(Collections.singleton(n4));
        result = cu.cartesianProduct(neighborhood1);

        assertEquals(1, result.size());
        assertEquals(route2, result.iterator().next());

        INode<Integer> n6 = mock(INode.class);
        stub(n6.getNeighbors()).toReturn(Collections.EMPTY_MAP);

        neighborhood1.set(neighborhood1.size()-1, neighborhood2);
        neighborhood1.add(Collections.singleton(n6));
        result = cu.cartesianProduct(neighborhood1);//should return empty set as node 6 isn't connected to any other nodes

        assertTrue(result.isEmpty());
    }
}
