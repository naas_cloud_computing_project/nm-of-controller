package net.floodlightcontroller.nm.xml;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;


/**
 * Created by chemo_000 on 9/18/2015.
 */
public class LinkConfigParserTest
{
    private LinkConfigParser cp = new LinkConfigParser();

    private String testXML = "<link-config>\n"+
                            "    <link src=\"1\" dst=\"2\" cost=\"3.5\"/>\n"+
                            "    <link src=\"10.0.0.4\" dst=\"00:00:00:00\" capacity=\"10\"/>\n"+
                            "</link-config>";

    private InputStream fXmlStream;

    @Before
    public void setUp()
    {
         fXmlStream = new ByteArrayInputStream(testXML.getBytes(StandardCharsets.UTF_8));
    }

    @Test
    public void parseFile_shouldReadConfig_ifItIsCorrect() throws IOException, SAXException, ParserConfigurationException
    {
        cp.parseFile(fXmlStream);

        assertEquals(3.5, cp.getLinkCost(new ImmutablePair<>("1","2")).doubleValue());
        assertEquals(10.0, cp.getLinkCapacity(new ImmutablePair<>("10.0.0.4","00:00:00:00")).doubleValue());
        assertNull(cp.getLinkCapacity(new ImmutablePair<>("1","2")));
        assertNull(cp.getLinkCost(new ImmutablePair<>("10.0.0.4","00:00:00:00")));
    }
}
