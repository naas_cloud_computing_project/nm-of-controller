package net.floodlightcontroller.nm.xml;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static junit.framework.Assert.assertEquals;


/**
 * Created by chemo_000 on 9/18/2015.
 */
public class ConfigParserTest
{
    private ConfigParser cp = new ConfigParser();

    private String testXML = "<nm-config>\n"+
                            "    <queue_id_bw_ratio val=\"3\"/>\n"+
                            "    <max_switch_to_switch_bw val=\"4\"/>\n"+
                            "    <max_host_to_switch_bw val=\"5\"/>\n"+
                             "</nm-config>";

    private InputStream fXmlStream;

    @Before
    public void setUp()
    {
         fXmlStream = new ByteArrayInputStream(testXML.getBytes(StandardCharsets.UTF_8));
    }

    @Test
    public void parseFile_shouldReadConfig_ifItIsCorrect() throws IOException, SAXException, ParserConfigurationException
    {
        cp.parseFile(fXmlStream);

        assertEquals(3, cp.BW_TO_QUEUE_ID_RATIO);
        assertEquals(5.0, cp.MAX_HOST_LINK_BW);
        assertEquals(4.0, cp.MAX_SWITCH_LINK_BW);
    }
}
