package net.floodlightcontroller.nm;

import net.floodlightcontroller.nm.mapping.routing.NeighborhoodBuilderTest;
import net.floodlightcontroller.nm.allocation.RouteBrokerTest;
import net.floodlightcontroller.nm.mapping.routing.RouteEstimatorTest;
import net.floodlightcontroller.nm.utils.CartesianUtilsTest;
import net.floodlightcontroller.nm.xml.ConfigParserTest;
import net.floodlightcontroller.nm.xml.LinkConfigParserTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        RouteEstimatorTest.class,
        NeighborhoodBuilderTest.class,
        RouteBrokerTest.class,
        CartesianUtilsTest.class,
        ConfigParserTest.class,
        LinkConfigParserTest.class
})
public class NMTestSuite
{
}
