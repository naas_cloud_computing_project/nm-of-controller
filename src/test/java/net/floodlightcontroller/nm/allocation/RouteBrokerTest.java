package net.floodlightcontroller.nm.allocation;

import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.*;

/**
 * Created by chemo_000 on 2/9/2015.
 */
public class RouteBrokerTest
{
    private INode<Integer> n1;
    private INode<Integer> n2;
    private INode<Integer> n3;
    private INode<Integer> n4;
    private INode<Integer> n5;

    private ILink l12;
    private ILink l13;
    private ILink l24;
    private ILink l35;
    private ILink l54;

    private List<INode<Integer>> route1;
    private List<INode<Integer>> route2;

    @Before
    public void setUp()
    {
        this.l12 = mock(ILink.class);
        stub(l12.getBw()).toReturn(3.0);
        this.l13 = mock(ILink.class);
        stub(l13.getBw()).toReturn(4.0);
        this.l24 = mock(ILink.class);
        stub(l24.getBw()).toReturn(6.0);
        this.l35 = mock(ILink.class);
        stub(l35.getBw()).toReturn(8.0);
        this.l54 = mock(ILink.class);
        stub(l54.getBw()).toReturn(9.0);

        this.n1 = mock(INode.class);
//        stub(n1.getID()).toReturn(1);
        this.n2 = mock(INode.class);
//        stub(n2.getID()).toReturn(2);
        this.n3 = mock(INode.class);
//        stub(n3.getID()).toReturn(3);
        this.n4 = mock(INode.class);
//        stub(n4.getID()).toReturn(4);
        this.n5 = mock(INode.class);
//        stub(n5.getID()).toReturn(5);

        Map<INode<Integer>, ILink> neighbors1 = mock(Map.class);
        stub(neighbors1.get(n2)).toReturn(l12);
        stub(neighbors1.get(n3)).toReturn(l13);

        Map<INode<Integer>, ILink> neighbors2 = mock(Map.class);
        stub(neighbors2.get(n1)).toReturn(l12);
        stub(neighbors2.get(n4)).toReturn(l24);

        Map<INode<Integer>, ILink> neighbors3 = mock(Map.class);
        stub(neighbors3.get(n1)).toReturn(l13);
        stub(neighbors3.get(n5)).toReturn(l35);

        Map<INode<Integer>, ILink> neighbors4 = mock(Map.class);
        stub(neighbors4.get(n2)).toReturn(l24);
        stub(neighbors4.get(n5)).toReturn(l54);

        Map<INode<Integer>, ILink> neighbors5 = mock(Map.class);
        stub(neighbors5.get(n3)).toReturn(l35);
        stub(neighbors5.get(n4)).toReturn(l54);


        stub(n1.getNeighbors()).toReturn(neighbors1);
        stub(n2.getNeighbors()).toReturn(neighbors2);
        stub(n3.getNeighbors()).toReturn(neighbors3);
        stub(n4.getNeighbors()).toReturn(neighbors4);
        stub(n5.getNeighbors()).toReturn(neighbors5);

        route1 = new LinkedList<INode<Integer>>()
        {
            {
                add(n1);
                add(n2);
                add(n4);
            }
        };

        route2 = new LinkedList<INode<Integer>>()
        {
            {
                add(n1);
                add(n3);
                add(n5);
                add(n4);
            }
        };
    }

    @Test
    public void reserveResources_shouldConsequentlyReserveCostForEachPairOfNodes_onTheRoute()
    {
        IResourceBroker mockIRB = mock(IResourceBroker.class);
        RouteBroker rb = new RouteBroker(mockIRB);
        double cost =2.0;
        rb.reserveResources(route1, cost);

        verify(mockIRB, times(1)).allocateSpecifiedBandwidth(n1,n4,n1,n2,l12,cost);
        verify(mockIRB, times(1)).allocateSpecifiedBandwidth(n1,n4,n2,n4,l24,cost);
        verify(mockIRB, times(0)).allocateSpecifiedBandwidth(n1,n4,n1,n3,l13,cost);
        verify(mockIRB, times(0)).allocateSpecifiedBandwidth(n1,n4,n3,n5,l35,cost);
        verify(mockIRB, times(0)).allocateSpecifiedBandwidth(n1,n4,n5,n4,l54,cost);

        rb.reserveResources(route2, cost);

        verify(mockIRB, times(1)).allocateSpecifiedBandwidth(n1,n4,n1,n3,l13,cost);
        verify(mockIRB, times(1)).allocateSpecifiedBandwidth(n1,n4,n3,n5,l35,cost);
        verify(mockIRB, times(1)).allocateSpecifiedBandwidth(n1,n4,n5,n4,l54,cost);
    }

    @Test
    public void releaseResources_shouldConsequentlyReleaseResourcesForEachPairOfNodes_onTheRoute()
    {
        IResourceBroker mockIRB = mock(IResourceBroker.class);
        RouteBroker rb = new RouteBroker(mockIRB);
        double cost =2.0;
        rb.releaseSpecifiedResources(route1, cost);

        verify(mockIRB, times(1)).releaseSpecifiedBandwidth(n1,n4,n1,n2,l12,cost);
        verify(mockIRB, times(1)).releaseSpecifiedBandwidth(n1,n4,n2,n4,l24,cost);
        verify(mockIRB, times(0)).releaseSpecifiedBandwidth(n1,n4,n1,n3,l13,cost);
        verify(mockIRB, times(0)).releaseSpecifiedBandwidth(n1,n4,n3,n5,l35,cost);
        verify(mockIRB, times(0)).releaseSpecifiedBandwidth(n1,n4,n5,n4,l54,cost);

        rb.releaseSpecifiedResources(route2, cost);

        verify(mockIRB, times(1)).releaseSpecifiedBandwidth(n1,n4,n1,n3,l13,cost);
        verify(mockIRB, times(1)).releaseSpecifiedBandwidth(n1,n4,n3,n5,l35,cost);
        verify(mockIRB, times(1)).releaseSpecifiedBandwidth(n1,n4,n5,n4,l54,cost);
    }
}
