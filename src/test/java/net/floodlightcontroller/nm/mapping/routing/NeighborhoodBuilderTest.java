package net.floodlightcontroller.nm.mapping.routing;

import net.floodlightcontroller.nm.discovery.model.ILink;
import net.floodlightcontroller.nm.discovery.model.INode;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;

/**
 * Created by chemo_000 on 2/6/2015.
 */
public class NeighborhoodBuilderTest
{
    private INode<Integer> n1;
    private INode<Integer> n2;
    private INode<Integer> n3;
    private INode<Integer> n4;
    private INode<Integer> n5;

    private ILink l12;
    private ILink l13;
    private ILink l24;
    private ILink l35;
    private ILink l54;

    @Before
    public void setUp()
    {
        this.l12 = mock(ILink.class);
        stub(l12.getBw()).toReturn(3.0);
        this.l13 = mock(ILink.class);
        stub(l13.getBw()).toReturn(4.0);
        this.l24 = mock(ILink.class);
        stub(l24.getBw()).toReturn(6.0);
        this.l35 = mock(ILink.class);
        stub(l35.getBw()).toReturn(8.0);
        this.l54 = mock(ILink.class);
        stub(l54.getBw()).toReturn(9.0);

        this.n1 = mock(INode.class);
        stub(n1.getID()).toReturn(1);
        this.n2 = mock(INode.class);
        stub(n2.getID()).toReturn(2);
        this.n3 = mock(INode.class);
        stub(n3.getID()).toReturn(3);
        this.n4 = mock(INode.class);
        stub(n4.getID()).toReturn(4);
        this.n5 = mock(INode.class);
        stub(n5.getID()).toReturn(5);

        Map<INode<Integer>, ILink> neighbors1 = mock(Map.class);
        stub(neighbors1.keySet()).toReturn(new HashSet<INode<Integer>>()
        {{
                add(n2);
                add(n3);
            }});
        stub(neighbors1.get(n2)).toReturn(l12);
        stub(neighbors1.get(n3)).toReturn(l13);

        Map<INode<Integer>, ILink> neighbors2 = mock(Map.class);
        stub(neighbors2.keySet()).toReturn(new HashSet<INode<Integer>>()
        {{
                add(n1);
                add(n4);
            }});
        stub(neighbors2.get(n1)).toReturn(l12);
        stub(neighbors2.get(n4)).toReturn(l24);

        Map<INode<Integer>, ILink> neighbors3 = mock(Map.class);
        stub(neighbors3.keySet()).toReturn(new HashSet<INode<Integer>>()
        {{
                add(n1);
                add(n5);
            }});
        stub(neighbors3.get(n1)).toReturn(l13);
        stub(neighbors3.get(n5)).toReturn(l35);

        Map<INode<Integer>, ILink> neighbors4 = mock(Map.class);
        stub(neighbors4.keySet()).toReturn(new HashSet<INode<Integer>>()
        {{
                add(n2);
                add(n5);
            }});
        stub(neighbors4.get(n2)).toReturn(l24);
        stub(neighbors4.get(n5)).toReturn(l54);

        Map<INode<Integer>, ILink> neighbors5 = mock(Map.class);
        stub(neighbors5.keySet()).toReturn(new HashSet<INode<Integer>>()
        {{
                add(n4);
                add(n3);
            }});
        stub(neighbors5.get(n3)).toReturn(l35);
        stub(neighbors5.get(n4)).toReturn(l54);


        stub(n1.getNeighbors()).toReturn(neighbors1);
        stub(n2.getNeighbors()).toReturn(neighbors2);
        stub(n3.getNeighbors()).toReturn(neighbors3);
        stub(n4.getNeighbors()).toReturn(neighbors4);
        stub(n5.getNeighbors()).toReturn(neighbors5);
    }

    @Test
    public void buildNextNeighborhoods_shouldReturnNeighborhoods_whichContainDstNode_onlyInLastOne()
    {
        NeighborhoodBuilder nb = new NeighborhoodBuilder();
        List<Set<INode<Integer>>> neighborhoods = nb.buildNextNeighborhoods(n4, new LinkedList()
        {{
                add(Collections.singleton(n1));
            }}, 0);

        int length = neighborhoods.size();

        assertTrue(length > 0);

        for (int i = 0; i < length - 1; i++)
            assertFalse(neighborhoods.get(i).contains(n4));

        assertTrue(neighborhoods.get(length - 1).contains(n4));
    }

    @Test(expected = RuntimeException.class)
    public void buildNextNeighborhoods_shouldThrowRuntimeException_forUnreachableDest()//todo make known exception for NBuilder and proper handling
    {
        NeighborhoodBuilder nb = new NeighborhoodBuilder();
        INode<Integer> n6 = mock(INode.class);

        nb.buildNextNeighborhoods(n6, new LinkedList()
        {{
                add(Collections.singleton(n1));
            }}, 0);
    }

    @Test
    public void addNextNeighborhood_shouldReturnPreviousNeighborhoodsWithNewOne()
    {
        NeighborhoodBuilder nb = new NeighborhoodBuilder();
        List<Set<INode<Integer>>> neighborhoods = new ArrayList<Set<INode<Integer>>>()
        {{
                add(Collections.singleton(n1));
                add(new HashSet<INode<Integer>>()
                {{
                        add(n2);
                        add(n3);
                    }});
                add(new HashSet<INode<Integer>>()
                {{
                        add(n1);
                        add(n4);
                        add(n5);
                    }});
            }};
        int prevLength = neighborhoods.size();

        nb.addNextNeighborhood(neighborhoods);

        int length = neighborhoods.size();

        assertTrue(length == prevLength + 1);

        for (int i = 0; i < length - 2; i++)
            assertFalse(neighborhoods.get(i).contains(n4));

        assertTrue(neighborhoods.get(length - 1).contains(n4));

        assertFalse(neighborhoods.get(length - 1).contains(n1));
        assertTrue(neighborhoods.get(length - 1).contains(n2));
        assertTrue(neighborhoods.get(length - 1).contains(n3));
        assertTrue(neighborhoods.get(length - 1).contains(n4));
        assertTrue(neighborhoods.get(length - 1).contains(n5));
    }
}
