import net.floodlightcontroller.core.OFConnectionTest;
import net.floodlightcontroller.core.OFSwitchBaseTest;
import net.floodlightcontroller.core.internal.*;
import net.floodlightcontroller.core.util.AppCookieTest;
import net.floodlightcontroller.core.util.MessageDispatcherTest;
import net.floodlightcontroller.core.util.SingletonTaskTest;
import net.floodlightcontroller.debugcounter.DebugCounterImplTest;
import net.floodlightcontroller.debugcounter.DebugCounterServiceTest;
import net.floodlightcontroller.debugcounter.OFConnectionCountersTest;
import net.floodlightcontroller.debugevent.DebugEventTest;
import net.floodlightcontroller.debugevent.EventTest;
import net.floodlightcontroller.devicemanager.internal.DeviceManagerImplTest;
import net.floodlightcontroller.devicemanager.internal.DeviceUniqueIndexTest;
import net.floodlightcontroller.firewall.FirewallTest;
import net.floodlightcontroller.forwarding.ForwardingTest;
import net.floodlightcontroller.hub.HubTest;
import net.floodlightcontroller.learningswitch.LearningSwitchTest;
import net.floodlightcontroller.linkdiscovery.internal.LinkDiscoveryManagerTest;
import net.floodlightcontroller.loadbalancer.LoadBalancerTest;
import net.floodlightcontroller.notification.NotificationTest;
import net.floodlightcontroller.packet.*;
import net.floodlightcontroller.routing.RouteTest;
import net.floodlightcontroller.staticflowentry.StaticFlowTests;
import net.floodlightcontroller.storage.memory.tests.MemoryStorageTest;
import net.floodlightcontroller.topology.TopologyInstanceTest;
import net.floodlightcontroller.topology.TopologyManagerTest;
import net.floodlightcontroller.util.EnumBitmapsTest;
import net.floodlightcontroller.util.OFMessageDamperTest;
import net.floodlightcontroller.util.TimedCacheTest;
import net.floodlightcontroller.virtualnetwork.VirtualNetworkFilterTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.sdnplatform.sync.VersionedTest;
import org.sdnplatform.sync.client.ClientTest;
import org.sdnplatform.sync.internal.BootstrapTest;
import org.sdnplatform.sync.internal.SyncManagerTest;
import org.sdnplatform.sync.internal.store.InMemoryStorageEngineTest;
import org.sdnplatform.sync.internal.store.JacksonStoreTest;
import org.sdnplatform.sync.internal.store.JavaDBStorageEngineTest;
import org.sdnplatform.sync.internal.store.RemoteStoreTest;
import org.sdnplatform.sync.internal.version.ClockEntryTest;
import org.sdnplatform.sync.internal.version.VectorClockInconsistencyResolverTest;
import org.sdnplatform.sync.internal.version.VectorClockTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ControllerTest.class,
        OFChannelHandlerVer10Test.class,
        OFChannelHandlerVer13Test.class,
        OFSwitchHandshakeHandlerVer10Test.class,
        OFSwitchHandshakeHandlerVer13Test.class,
        OFSwitchManagerTest.class,
        OFSwitchTest.class,
        RoleManagerTest.class,
        AppCookieTest.class,
        MessageDispatcherTest.class,
        SingletonTaskTest.class,
        OFConnectionTest.class,
        OFSwitchBaseTest.class,
        DebugCounterImplTest.class,
        DebugCounterServiceTest.class,
        OFConnectionCountersTest.class,
        DebugEventTest.class,
        EventTest.class,
        DeviceManagerImplTest.class,
        DeviceUniqueIndexTest.class,
        FirewallTest.class,
        ForwardingTest.class,
        HubTest.class,
        LearningSwitchTest.class,
        LinkDiscoveryManagerTest.class,
        LoadBalancerTest.class,
        NotificationTest.class,
        BSNTest.class,
        DHCPTest.class,
        EthernetTest.class,
        ICMPTest.class,
        IPv4Test.class,
        LLDPOrganizationalTLVTest.class,
        LLDPTest.class,
        PacketTest.class,
        TCPTest.class,
        UDPTest.class,
        RouteTest.class,
        StaticFlowTests.class,
        MemoryStorageTest.class,
        TopologyInstanceTest.class,
        TopologyManagerTest.class,
        EnumBitmapsTest.class,
        OFMessageDamperTest.class,
        TimedCacheTest.class,
        VirtualNetworkFilterTest.class,
        ClientTest.class,
        InMemoryStorageEngineTest.class,
        JacksonStoreTest.class,
        JavaDBStorageEngineTest.class,
        RemoteStoreTest.class,
        ClockEntryTest.class,
        VectorClockInconsistencyResolverTest.class,
        VectorClockTest.class,
        BootstrapTest.class,
        SyncManagerTest.class,
        VersionedTest.class
})
public class FloodlightTestSuite
{
}
