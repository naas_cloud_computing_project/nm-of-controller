NM module for Floodlight OpenFlow Controller v1.0 README
Updated Sept 26, 2016 by Dmitrii Chemodanov

All feedback appreciated to dycbt4@mail.missouri.edu 

@copyright 2016 VIMAN laboratory, Computer Science Department, University of Missouri-Columbia.
=================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


What is NM and NM module
================
NM is a module of the Floodlight OpenFlow controller (http://www.projectfloodlight.org/floodlight/) and it allocates user virtual link requests over physical OpenFlow network by computing the constrained shortest path. Details of the NM algorithm (viz. "Neighborhood Method") can be found at http://faculty.missouri.edu/calyamp/publications/nm_and_routing-lanman16.pdf

DISTRIBUTION
================
The distribution tree contains: 

README.TXT
	--this file
build.xml
	--used by ant
logback.xml
	--used for controller log options
link-config.xml
	--config file for NM module
nm-config.xml
    --config file for NM module
src/ 
	--source files
src/main/java/net/floodlightcontroller/nm/
	--source files of NM module
src/test/java/net/floodlightcontroller/nm/
    --source files of NM module tests
lib/
	--required java libraries
html/
	--user web interface to interact with NM module (to be placed in respective directory of an application server)
experiment_settings/
	--settings for experiment in GENI testbed (including rspecs, test procedures and results obtained for NM paper)
	


COMPILATION AND RUN
============
Compiling and run of this package requires Ant, Java 1.7, Apache (or other application server) with PHP and cURL installed. These can be downloaded respectively from:  
http://jakarta.apache.org/ant/index.html 
http://java.sun.com/j2se/
http://httpd.apache.org/
http://curl.haxx.se/


- compile
     ant 
- clean
     ant clean 
- start Floodlight controller with NM module
     java -jar target/floodlight.jar (alternatively, ant run)

- visit User Web Interface at:
	[controller ip]/index.php (login: admin password: any)

- use NM with newer Floodlight versions:
    Its possible to use provided NM module with newer versions of Floodlight controller. To this end, you need:

    - download and install recent Floodlight controller as described here - https://floodlight.atlassian.net/wiki/display/floodlightcontroller/Installation+Guide

    - copy NM module source code (available at src/main/java/net/floodlightcontroller/nm/) to src/main/java/net/floodlightcontroller/

    - [optional] copy nm-config.xml and link-config.xml to controller directory

    - [optional] copy NM module tests source code (available at src/test/java/net/floodlightcontroller/nm/) to src/test/java/net/floodlightcontroller/

    - register NM module within Floodlight controller by adding 'net.floodlightcontroller.nm.openflow.controller.NMControllerV01' to src/main/resources/META-INF/services/net.floodlightcontroller.core.module.IFloodlightModule
    and src/main/resources/floodlightdefault.properties before forwarding module (net.floodlightcontroller.forwarding.Forwarding).
    More details are available at https://floodlight.atlassian.net/wiki/display/floodlightcontroller/How+to+Write+a+Module.

    - add following additional libraries to both build.xml and your controller lib/ folder (note: they can be downloaded from current lib/ folder):
        *commons-lang3-3.3.2.jar or newer
        *heaps-2.0.jar or newer
        *mockito-all-1.9.5.jar or newer

    - recompile Floodlight controller using ant clean/ant commands

    If NM was successfully included in Floodlight controller, you should see something similar to the following:
        admin@c:~/nm-controller$ java -jar target/floodlight.jar
        13:36:42.687 INFO [n.f.c.m.FloodlightModuleLoader:main] Loading modules from src/main/resources/floodlightdefault.properties
        13:36:43.053 INFO [n.f.n.x.ConfigParser:main] [NM-XMLParser] XML Parser successfully read next values:
        BW_TO_QUEUE_ID_RATIO = 1
        MAX_HOST_LINK_BW = 10.0
        MAX_SWITCH_LINK_BW = 10.0
        FLOW_IDLE_TIMEOUT = 60
        FLOW_HARD_TIMEOUT = 0
        ROUTING_TYPE = NM
        EXPORT_RESULTS = false
        DYNAMIC_GRAPH = false
        ...

     
CONFIGURATION
============
link-config.xml -- used for NM module to specify link properties (e.g., capacity or an arbitrary cost)
nm-config.xml -- used for NM module to specify basic properties (e.g., flow duration)