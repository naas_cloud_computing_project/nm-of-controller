Module for the Floodlight OpenFlow controller (url: http://www.projectfloodlight.org/floodlight/) with implemented Neighborhood Method, Extended Dijkstra, Iterative Bellman-Ford and Exhaustive Breadth-First Search routing algorithms. 

The "NM-module" reads link metrics (e.g., capacity and cost) from an XML configuration file (link-config.xml) as well as other optional properties (e.g., the flow duration time) from another XML configuration file (nm-config.xml). 

Upon virtual link allocation, the NM module sets appropriate flow rules within OpenFlow switches along the computed path and then assigns these flows to a pre-configured queue for bandwidth
provisioning. 

To estimate link available bandwidth, the NM module uses both its capacity information derived from the XML configuration file and its allocated bandwidth derived from an information about flows stored in OpenFlow switches.

To request virtual links, one can used a graphical web interface (written in php and stored in html/ folder) which uses a RESTful API to communicate with the NM-module.