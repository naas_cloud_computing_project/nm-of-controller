<?php
    $tab = "status";
    require_once("./includes/header.php");
?>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3>Status of Links</h3>
                </div>
                <div class="col-md-6">
                    <span class="btn btn-default btn-sm pull-right" style="margin-top: 20px;"><span class="glyphicon glyphicon-refresh"></span> Refresh</span>
                </div>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Source IP</th>
                        <th>Destination IP</th>
                        <th>Allocated Bandwidth (Mbps)</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="warning">
                        <td>124.25.68.91</td>
                        <td>234.21.54.108</td>
                        <td>12</td>
                        <td>Pending</td>
                    </tr>
                    <tr class="success">
                        <td>193.24.21.10</td>
                        <td>23.49.121.9</td>
                        <td>10</td>
                        <td>Allocated</td>
                    </tr>
                    <tr class="success">
                        <td>201.32.42.93</td>
                        <td>129.39.48.13</td>
                        <td>5</td>
                        <td>Allocated</td>
                    </tr>
                    <tr class="danger">
                        <td>10.34.83.92</td>
                        <td>43.89.248.102</td>
                        <td>20</td>
                        <td>Failed</td>
                    </tr>
                    <tr class="success">
                        <td>65.87.55.125</td>
                        <td>95.54.124.29</td>
                        <td>20</td>
                        <td>Allocated</td>
                    </tr>
                </tbody>
            </table>
            <p>
                Total Allocated Bandwidth: 35/50 Mbps
            </p>
        </div>

<?php require_once("./includes/footer.php"); ?>