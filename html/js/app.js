
var w = window,
    d = document,
    e = d.documentElement,
    g = d.getElementsByTagName('body')[0],
    x = w.innerWidth || e.clientWidth || g.clientWidth,
    y = w.innerHeight|| e.clientHeight|| g.clientHeight,
    xpad = 0;

// set up SVG for D3
var width  = 960,
    height = 750,
    colors = d3.scale.category10();

// set xpad
if(x <= width)
{
	xpad = 0;
}
else
{
	xpad = ((x-width)/2);
}    


var svg = d3.select('#diagram .graph')
  .append('svg')
  .attr('width', width)
  .attr('height', height);


function updateWindow(){
    x = w.innerWidth || e.clientWidth || g.clientWidth;
    y = w.innerHeight|| e.clientHeight|| g.clientHeight;
    // alert(x + ' × ' + y);
    //svg.attr("width", x).attr("height", y);
    if(x <= width)
    {
        xpad = 0;
    }
    else
    {
        xpad = ((x-width)/2);
    }
}
window.onresize = updateWindow;

// set up initial nodes and links
//  - nodes are known by 'id', not by index in array.
//  - reflexive edges are indicated on the node (as a bold black circle).
//  - links are always source < target; edge directions are set by 'left' and 'right'.
var blue = '#1F77B4'
var red = '#D62728'

var nodes = [];
var links = [];
var networkPaths = [];
var lastNodeId = 0;
var lastLinkId = 0;
d3.json("./proxy.php?request=admin_physical_links", function(error, json) {
  if (error) return console.warn(error);
  data = json;
  console.dir(data);
  //create nodes
  data.forEach(function(d) {
  	   var ifSExists = 0;
  	   var ifDExists = 0;
	   for(var ii = 0; ii < nodes.length; ii++)
	   {
	   		if(d.source === nodes[ii].source)
	   		{
	   			ifSExists++;
	   		}
	   		if(d.destination === nodes[ii].source)
	   		{
	   			ifDExists++;
	   		}
	   }
	   if(ifSExists == 0)
	   {
	   		var node = {id: lastNodeId++, type: "Switch", x: 250, y: 500, source: d.source, color: blue}
	   		if(d.src_type == "HOST")
	   		{
	   			node.type = "Host";
	   			node.color = red;
	   		}
	   		nodes.push(node);
	   }
	   if(ifDExists == 0)
	   {
	   		var node = {id: lastNodeId++, type: "Switch", x: 250, y: 500, source: d.destination, color: blue}
	   		if(d.dst_type == "HOST")
	   		{
	   			node.type = "Host";
	   			node.color = red;
	   		}
	   		nodes.push(node);
	   }
  })
  //create paths
  data.forEach(function(d) {
  	var link = {id: lastLinkId++, bandwidth: d.bandwidth, capacity: d.capacity, cost: d.cost}; 
    for(var ii = 0; ii < nodes.length; ii++)
    {  
		if(d.source === nodes[ii].source) link.source = nodes[ii];
		if(d.destination === nodes[ii].source) link.target = nodes[ii];
	}
	links.push(link);
  })
  
  
  console.dir(nodes);
  console.dir(links);

//BEGIN PROGRAM CODE HERE 
/*OLD STATIC NODE CODE
var nodes = [
    {id: 0, type: 'host', ip: '128.0.0.1', color: red},
    {id: 1, type: 'switch', ip: '128.0.0.2', color: blue},
    {id: 2, type: 'switch', ip: '128.0.0.3', color: blue},
    {id: 3, type: 'switch', ip: '128.0.0.4', color: blue},
    {id: 4, type: 'switch', ip: '128.0.0.5', color: blue},
    {id: 5, type: 'host', ip: '128.0.0.6', color: red},
    {id: 6, type: 'switch', ip: '128.0.0.7', color: blue}
  ],
  lastNodeId = 6,
  links = [
    {id: 0, source: nodes[0], target: nodes[1], left: false, right: false },
    {id: 1, source: nodes[0], target: nodes[2], left: false, right: false },
    {id: 2, source: nodes[1], target: nodes[3], left: false, right: false },
    {id: 3, source: nodes[2], target: nodes[4], left: false, right: false },
    {id: 4, source: nodes[3], target: nodes[5], left: false, right: false },
    {id: 5, source: nodes[4], target: nodes[6], left: false, right: false },
    {id: 6, source: nodes[6], target: nodes[5], left: false, right: false },
    {id: 7, source: nodes[1], target: nodes[2], left: false, right: false },
    {id: 8, source: nodes[3], target: nodes[4], left: false, right: false }
  ],
   networkPaths = [
    {id: 0, pathNodes: [nodes[0],nodes[1],nodes[3],nodes[5]]},
    {id: 1, pathNodes: [nodes[0],nodes[2],nodes[4],nodes[6],nodes[5]]}
   ],*/
var networkLinkcount = 0;
console.dir(nodes); 
// init D3 force layout
var force = d3.layout.force()
    .nodes(nodes)
    .links(links)
    .size([width, height])
    .linkDistance(100)
    .charge(-(lastNodeId * 600)) //seperation of d3 nodess
    //.friction(.1)
    //.linkStrength(9)
    .on('tick', tick)

// define arrow markers for graph links
svg.append('svg:defs').append('svg:marker')
    .attr('id', 'end-arrow')
    .attr('viewBox', '0 -5 10 10')
    .attr('refX', 6)
    .attr('markerWidth', 3)
    .attr('markerHeight', 3)
    .attr('orient', 'auto')
  .append('svg:path')
    .attr('d', 'M0,-5L10,0L0,5')
    .attr('fill', '#000');  //color of arrows

svg.append('svg:defs').append('svg:marker')
    .attr('id', 'start-arrow')
    .attr('viewBox', '0 -5 10 10')
    .attr('refX', 4)
    .attr('markerWidth', 3)
    .attr('markerHeight', 3)
    .attr('orient', 'auto')
  .append('svg:path')
    .attr('d', 'M10,-5L0,0L10,5')
    .attr('fill', '#000');  //color of arrows

// line displayed when dragging new nodes
var drag_line = svg.append('svg:path')
  .attr('class', 'link dragline hidden')
  .attr('d', 'M0,0L0,0');

// handles to link and node element groups
var path = svg.append('svg:g').selectAll('path'),
    circle = svg.append('svg:g').selectAll('g');

// mouse event vars
var selected_node = null,
    selected_link = null,
    mousedown_link = null,
    mousedown_node = null,
    mouseup_node = null;

function resetMouseVars() {
  mousedown_node = null;
  mouseup_node = null;
  mousedown_link = null;
}

// Display IP strings
function makeIPString(node) {
  /*var vals = d.vals,
      outputVars = [];

  for(var i = 0; i < varCount; i++) {
   ƒ // attach 'not' symbol to false values
    outputVars.push((vals[i] ? 'poop' : '\u00ac') + propvars[i]);
  }*/
  console.dir(node);
  return node.source; //outputVars.join('Ip');
}

// Display Bandwidth Strings
function makeBandwidthString(node) {
  return node.bandwidth; //node.bandwidth
}
// set # of vars currently in use and notify panel of changes
function networkLink(count) {
  networkLinkcount = count;

  // update variable count button states
  /*varCountButtons.each(function(d,i) {
    if(i !== varCount-1) d3.select(this).classed('active', false);
    else d3.select(this).classed('active', true);
  });

  //update graph text
  circle.selectAll('text:not(.id)').text(makeAssignmentString);*/
  //update graph link color
  //path.selectAll('style:not(.id)').style('stroke', function(d) { return d.id == count ? ('#0f0') : ('#000') }) //color of link
  console.dir(count);
  restart();
}


// update force layout (called automatically each iteration)
function tick() {
  // draw directed edges with proper padding from node centers
  path.attr('d', function(d) {
    var deltaX = d.target.x - d.source.x,
        deltaY = d.target.y - d.source.y,
        dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY),
        normX = deltaX / dist,
        normY = deltaY / dist,
        sourcePadding = d.left ? 24 : 20,
        targetPadding = d.right ? 24 : 20,
        sourceX = d.source.x + (sourcePadding * normX),
        sourceY = d.source.y + (sourcePadding * normY),
        targetX = d.target.x - (targetPadding * normX),
        targetY = d.target.y - (targetPadding * normY);
    return 'M' + sourceX + ',' + sourceY + 'L' + targetX + ',' + targetY;
  });

  circle.attr('transform', function(d) {
    return 'translate(' + d.x + ',' + d.y + ')';
  });
}

// update graph (called when needed)
function restart() {
    // path (link) group
    path = path.data(links);//, function(d) { return d.id; } );
   
   //update existed paths
    //path.classed('selected', function(d) { console.dir(d); return d === selected_link; })
   	path.style('stroke', function(d) { console.dir(networkPaths[1].id)
   										  var color = d3.rgb('#000');
   										  for(var i = 0; i < networkPaths[(networkLinkcount-1)].pathNodes.length; i++)
   										  {
   										  	if(d.source === networkPaths[(networkLinkcount-1)].pathNodes[i])
   										  	{
   										  		for(var j = 0; j < networkPaths[(networkLinkcount-1)].pathNodes.length; j++)
   										 		{
   										 			if(d.target === networkPaths[(networkLinkcount-1)].pathNodes[j])
   										 			{
   										 				color = d3.rgb('#0f0');
   										 			}
   										 		}
   										  	}
   										  }
   										  return color;
   									   	  /*var test = networkPaths[(networkLinkcount-1)].pathNodes.forEach(function(dP) {
   									   	      console.dir(dP);
											  console.dir(d.source);
											  (d.source === dP || d.destination === dP) ? console.dir('#0f0') : console.dir('#000');
   									   	      return (d.source === dP || d.destination === dP) ? d3.rgb('#0f0') : d3.rgb('#000');
   									   	    
   									   	  })  
   									   	  console.log(test);*/
   									 }) //color of link
    .style('marker-start', function(d) { return d.left ? 'url(#start-arrow)' : ''; })
    .style('marker-end', function(d) { return d.right ? 'url(#end-arrow)' : ''; });

  // add new links
  var g = path.enter().append('svg:path')
    .attr('class', 'link')
    .classed('selected', function(d) { return d === selected_link; })
    .style('marker-start', function(d) { return d.left ? 'url(#start-arrow)' : ''; })
    .style('marker-end', function(d) { return d.right ? 'url(#end-arrow)' : ''; })
    //.style('stroke', function(d){ return d3.rgb('#000')}) //color of link
    .style('stroke', d3.rgb('#000')) //color of link
    .on('mouseover', function(d) {
    
        var g = d3.select(this); // The node
        var point = d3.mouse(this);
        var div = d3.select('body').append('div')
        			.attr('class', 'tooltip')
                    .attr('pointer-events', 'none')
        			.style('padding', '8px')
                    .style("opacity", 1)
                    .html("Link " + "<br>" + 
                          "ID: " + d.id + "<br>" +
                          "Bandwidth: " + d.bandwidth + "<br>" +
                          "Capacity: " + d.capacity + "<br>" + 
						  "Cost: " + d.cost)
                    .style("left", (xpad + point[0] + 20 + "px"))
                    .style("top", (point[1] + 120 + "px"))
                    .style("width", "100px")
        if(!mousedown_node || d === mousedown_node) return;
        // enlarge target node
        d3.select(this).attr('transform', 'scale(1.1)');
    })
    .on('mouseout', function(d) {
        d3.select("body").select('div.tooltip').remove();
        if(!mousedown_node || d === mousedown_node) return;
        // unenlarge target node
        d3.select(this).attr('transform', '');
    })
  // remove old links
  path.exit().remove();

   
  // circle (node) group
  // NB: the function arg is crucial here! nodes are known by id, not by index!
  circle = circle.data(nodes, function(d) { return d.id; });

  // update existing nodes (reflexive & selected visual states)
  circle.selectAll('circle')
    .style('fill', function(d) { return (d === selected_node) ? d3.rgb(d.color).brighter().toString() : (d.color); })
    .classed('reflexive', function(d) { return d.reflexive; });

  // add new nodes
  var g = circle.enter().append('svg:g');

  g.append('svg:circle')
    .attr('class', 'node')
    .attr('r', 20)
    .style('fill', function(d) { return (d === selected_node) ? d3.rgb(d.color).brighter().toString() : (d.color);})
    .style('stroke', function(d) { return d3.rgb(d.color).darker().toString(); })
    .classed('reflexive', function(d) { return d.reflexive; })
    .on('mouseover', function(d) {
    
        var g = d3.select(this); // The node
        var div = d3.select('body').append('div')
        			.attr('class', 'tooltip')
                    .attr('pointer-events', 'none')
                    .style("opacity", 1)
                    .html("Node" + "<br>" + 
                          "ID: " + d.id + "<br>" +
                          "Type: " + d.type + "<br>" +
                          "Source: " + d.source)
                    .style("width", (d.type === "Host") ? "100px" : "180px")
                    .style("left", (xpad + d.x + 24 + "px"))
                    .style("top", (d.y + 104 + "px"))
    })
    .on('mouseout', function(d) {
        d3.select("body").select('div.tooltip').remove();
    })

  // show node IDs
  g.append('svg:text')
      .attr('x', 0)
      .attr('y', 4)
      .attr('class', 'id')
      .text(function(d) { return d.type; });
      
  // remove old nodes
  circle.exit().remove();

  // set the graph in motion
  force.start();
}

function mousedown() {
  // prevent I-bar on drag
  //d3.event.preventDefault();
  
  // because :active only works in WebKit?
  svg.classed('active', true);

  if(d3.event.ctrlKey || mousedown_node || mousedown_link) return;

  // insert new node at point
  var point = d3.mouse(this),
      node = {id: ++lastNodeId, reflexive: false};
  node.x = point[0];
  node.y = point[1];
  nodes.push(node);

  restart();
}

restart();
})