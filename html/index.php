<?php
    $tab = "links";
    $require_login = true;
    require_once("./includes/header.php");
?>        
<div id="request-link-modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-primary">Request Virtual Link</h3>
            </div>
            <div class="modal-body">
                <form>
                    <div id="new-link-request-container">
                    
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="btn-list">
                    <span id="cancel-link-request-btn" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign"></span> Cancel</span>
                    <span id="send-request-link-btn" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok-sign"></span> Send</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="request-constrained-link-modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-primary">Request Constrained Virtual Link</h3>
            </div>
            <div class="modal-body">
                <form>
                    <div id="new-constrained-link-request-container">
                    
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="btn-list">
                    <span id="cancel-link-request-btn" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign"></span> Cancel</span>
                    <span id="send-request-constrained-link-btn" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok-sign"></span> Send</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="remove-links-modal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!--<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title text-primary">Remove Network Links [administrator only]</h3>
            </div>-->
            <div class="modal-body" align="center">
                <h5 class="text-primary"><font color="red">[Administrator ONLY] WARNING: This action will remove ALL VLs for ALL users including administrator! Are you sure you want to continue?</font></h5>
            </div>
            <div class="modal-footer">
                <div class="btn-list">
                    <span id="cancel-link-request-btn" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign"></span> Cancel</span>
                    <span id="confirm-remove-links-btn" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok-sign"></span> Confirm</span>
                </div>
            </div>
        </div>
    </div>
</div>


        
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h3 class="text-primary">Virtual Links</h3>
                </div>
                <div class="col-md-5" style="margin-top: 10px;">
					<div id="bandwidth-box" class="text-primary">
						Total Allocated Bandwidth: <span id="network_link_allocated">0</span> / <span id="network_link_total">0</span> Mbps
					</div>
                </div>
                <div class="col-md-4 btn-list" style="margin-top: 20px;">
					<span id="delete-links-btn" class="btn btn-default btn-sm" data-toggle="modal" data-target="#remove-links-modal"><span class="glyphicon glyphicon-remove"></span></span>
                    <span id="refresh-links-btn" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-refresh"></span></span>
					<span id="request-constrained-link-btn" class="btn btn-default btn-sm" data-toggle="modal" data-target="#request-constrained-link-modal"><span class="glyphicon glyphicon-lock"></span></span>
                    <span id="request-link-btn" class="btn btn-default btn-sm" data-toggle="modal" data-target="#request-link-modal"><span class="glyphicon glyphicon-plus"></span></span>
                </div>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Source IP</th>
                        <th>Destination IP</th>
                        <th>Bandwidth (Mbps)</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody id="network_link_list" class="hidden">
                </tbody>
                <tbody id="network_link_loading" style="display: none;">
                  <tr>
                    <td colspan="4"><img src="./images/ajax-loader.gif" alt="Loading... /" class="center-block img-responsive"></td>
                  </tr>
                </tbody>
            </table>
        </div>
        
        <!-- TEMPLATE FOR NEW LINK REQUEST FORM ELEMENTS -->
        <div id="new-link-request-template" class="hidden">
            <div class="row row-border-top">
                <div class="col-md-4 form-group">
                    <label for="requestSource[]" class="pull-left control-label">Source IP:</label>
                        <input type="text" class="form-control request-link-source" name="requestSource[]" id="requestSource[]" />
                </div>
                <div class="col-md-4 form-group">
                    <label for="requestDest[]" class="pull-left control-label">Destination IP:</label>
                        <input type="text" class="form-control request-link-dest" name="requestDest[]" id="requestDest[]" />
                </div>
                <div class="col-md-2 form-group">
                    <label for="requestBandwidth[]" class="pull-left control-label">Bandwidth:</label>
                        <input type="text" class="form-control request-link-bandwidth" name="requestBandwidth[]" id="requestBandwidth[]" />
                </div>
                <div class="col-md-2">
                    <span class="btn-plus-request-link btn btn-default btn-sm"><span class="glyphicon glyphicon-plus"></span></span>
                    &nbsp; &nbsp; 
                    <span class="btn-minus-request-link btn btn-default btn-sm"><span class="glyphicon glyphicon-minus"></span></span>
                </div>
            </div>
        </div>
		
		<!-- TEMPLATE FOR NEW CONSTRAINED LINK REQUEST FORM ELEMENTS -->
        <div id="new-constrained-link-request-template" class="hidden">
            <div class="row row-border-top">
                <div class="col-md-3 form-group">
                    <label for="requestSource[]" class="pull-left control-label">Source IP:</label>
                        <input type="text" class="form-control request-link-source" name="requestSource[]" id="requestSource[]" />
                </div>
                <div class="col-md-3 form-group">
                    <label for="requestDest[]" class="pull-left control-label">Destination IP:</label>
                        <input type="text" class="form-control request-link-dest" name="requestDest[]" id="requestDest[]" />
                </div>
                <div class="col-md-2 form-group">
                    <label for="requestBandwidth[]" class="pull-left control-label">Bandwidth:</label>
                        <input type="text" class="form-control request-link-bandwidth" name="requestBandwidth[]" id="requestBandwidth[]" />
                </div>
				<div class="col-md-2 form-group">
                    <label for="requestCost[]" class="pull-left control-label">Cost:</label>
                        <input type="text" class="form-control request-link-cost" name="requestCost[]" id="requestCost[]" />
                </div>
                <div class="col-md-2">
                    <span class="btn-plus-request-constrained-link btn btn-default btn-sm"><span class="glyphicon glyphicon-plus"></span></span>
                    &nbsp; &nbsp; 
                    <span class="btn-minus-request-constrained-link btn btn-default btn-sm"><span class="glyphicon glyphicon-minus"></span></span>
                </div>
            </div>
        </div>
        
        <script>
            $(function() {
                var new_link_request_template = $("#new-link-request-template").html();
                var new_link_request_container = $("#new-link-request-container");
				
				var new_constrained_link_request_template = $("#new-constrained-link-request-template").html();
                var new_constrained_link_request_container = $("#new-constrained-link-request-container");
                
                // Add new link request template to container on load
                add_new_link_request();
				
				// Add new constrained link request template to container on load
                add_new_constrained_link_request();
                
                // Listener for when plus button is clicked on add link modal
                $("#new-link-request-container").on("click", ".btn-plus-request-link", function() {
                    add_new_link_request();
                });
                
                
                // Listener for when minus button is clicked on add link modal
                $("#new-link-request-container").on("click", ".btn-minus-request-link", function() {
                    $(this).closest(".row").fadeOut(function() {
                        $(this).remove();
                    });
                });
				
				// Listener for when plus button is clicked on add constrained link modal
                $("#new-constrained-link-request-container").on("click", ".btn-plus-request-constrained-link", function() {
                    add_new_constrained_link_request();
                });
                
                
                // Listener for when minus button is clicked on remove constrained link modal
                $("#new-constrained-link-request-container").on("click", ".btn-minus-request-constrained-link", function() {
                    $(this).closest(".row").fadeOut(function() {
                        $(this).remove();
                    });
                });
                
                // Listener for when Cancel button clicked
                $("#cancel-link-request-btn").on("click", function() {
                    //add_new_link_request(); // Add new blank template
                });
                
                function add_new_link_request() {
                    new_link_request_container.append(new_link_request_template);
                }
				
				function add_new_constrained_link_request() {
                    new_constrained_link_request_container.append(new_constrained_link_request_template);
                }
                
                // Update network link list table
                get_network_link_list();
                
                // Listener for refresh button
                $("#refresh-links-btn").on("click", function() {
                    get_network_link_list();
                });
                
				// Listener for remove button
                //$("#delete-links-btn").on("click", function() {
                    //remove_network_links();
					//get_network_link_list();
                //});
				
                // Listener for controller custom IP radio button
                $("#controllerIPCustomRadio").on("click", function() {
                  $("#controllerIPCustomInput").focus();
                });
                // Listener for controller custom IP text input
                $("#controllerIPCustomInput").on("focus", function() {
                  $("#controllerIPCustomRadio").prop( "checked", true );
                });
                
                $('#request-link-modal').on("show.bs.modal", function() {
                  new_link_request_container.empty(); // Clear container's contents
                  add_new_link_request();
                });
                
                // Listener for send request link button 
                $("#send-request-link-btn").on("click", function() {
                  $('#request-link-modal').modal('hide');
                  var temp_source = "", temp_dest = "", temp_bandwidth = "", temp_send = "";
                  $.each($("#new-link-request-container .row"), function(key, row) {
                    temp_source = $(row).find(".request-link-source").val();
                    temp_dest = $(row).find(".request-link-dest").val();
                    temp_bandwidth = $(row).find(".request-link-bandwidth").val();
                    temp_send = "{\"source\": \"" + temp_source + "\", \"destination\": \"" + temp_dest + "\", \"bandwidth\": \"" + temp_bandwidth + "\", \"cost\": 0\"" + "\", \"status\": \"PENDING\"}";
                    send_network_link_request(temp_source, temp_dest, temp_bandwidth, 0);
                  });
                });
				
				// Listener for send request link button 
                $("#send-request-constrained-link-btn").on("click", function() {
                  $('#request-constrained-link-modal').modal('hide');
                  var temp_source = "", temp_dest = "", temp_bandwidth = "", temp_send = "";
                  $.each($("#new-constrained-link-request-container .row"), function(key, row) {
                    temp_source = $(row).find(".request-link-source").val();
                    temp_dest = $(row).find(".request-link-dest").val();
                    temp_bandwidth = $(row).find(".request-link-bandwidth").val();
					temp_cost = $(row).find(".request-link-cost").val();
                    temp_send = "{\"source\": \"" + temp_source + "\", \"destination\": \"" + temp_dest + "\", \"bandwidth\": \"" + temp_bandwidth + "\", \"cost\": \""  + temp_cost + "\", \"status\": \"PENDING\"}";
                    send_network_link_request(temp_source, temp_dest, temp_bandwidth, temp_cost);
                  });
                });
				
				// Listener for remove request link button 
                $("#confirm-remove-links-btn").on("click", function() {
                  $('#remove-links-modal').modal('hide');
                    remove_network_links();
					get_network_link_list();
                });
                
            });
            

            
            var network_link_list = $("#network_link_list");
            var network_link_loading = $("#network_link_loading");
            var network_link_allocated = $("#network_link_allocated"), network_link_allocated_num;
            var network_link_total = $("#network_link_total"), network_link_total_num;
            var xmlHttp;
            
            function get_network_link_list() {
              network_link_list.fadeOut(function() {
                network_link_loading.fadeIn(function() {
                  xmlHttp = new XMLHttpRequest();
                  
                  xmlHttp.onload = function() {
                      network_link_allocated_num = 0;
                      network_link_total_num = 0;
                      if (xmlHttp.status == 200) {                      
                          // Get the response text
                          var response = xmlHttp.responseText;
                          response = JSON.parse(response);
                          
                          var htmlStr = "";
                          var current = null;
                          for (var i = 0; i<response.length; i++){ // Loop through network links to build tbody rows
                            current = response[i];
                            current.status = capitalizeFirstLetter(current.status)
                            switch(current.status) { // Change row color and total allocated bandwidth count based on status
                              case "Allocated":
                                htmlStr += "<tr class=\"success\">";
                                network_link_allocated_num += current.bandwidth;
                                break;
                              case "Failed":
                                htmlStr += "<tr class=\"danger\">";
                                break;
                              case "Pending":
                                htmlStr += "<tr class=\"warning\">";
                                break;
                              default:
                                htmlStr += "<tr>";
                            }
                            network_link_total_num += current.bandwidth;
                            htmlStr += "<td>" + current.source + "</td><td>" + current.destination + "</td><td>" + current.bandwidth + "</td><td>" + current.status;
                            if(current.status == "Failed") {
                              htmlStr += ' <span class="btn btn-danger btn-xs data-toggle="tooltip" data-placement="top" title="' + current.error  + '"><span class="glyphicon glyphicon-info-sign"></span></span>'; 
                            }
                            htmlStr += "</td></tr>";
                          }
                          network_link_list.html(htmlStr); // Update tbody's html                         
                          network_link_loading.fadeOut(function() { // Hide loading gif and display tbody with network link rows
                            network_link_list.removeClass("hidden").fadeIn();
                          });
                          update_bandwidth_counters();
                      }
                  };    
                  xmlHttp.open("GET", "proxy.php?request=user_virtual_links", true);
                  xmlHttp.send();
                });
              });

            }
            
			function remove_network_links() {              
                  xmlHttp.open("GET", "proxy.php?request=remove_all_vls", true);
                  xmlHttp.send();    
            }
            
            function send_network_link_request(s, d, b, c) {
                  var rowId = s + d + b;
                  rowId = rowId.replace(/[^0-9]+/g, '');
                  add_network_link_row(rowId, s, d, b);
                  xmlHttp = new XMLHttpRequest();
                  xmlHttp.onload = function() {
                      if (xmlHttp.status == 200) {
                        var response = xmlHttp.responseText, statusClass = "";
                        response = JSON.parse(response);
                        response = response[0];
                        response.status = capitalizeFirstLetter(response.status)
                        switch(response.status) { // Change row color based on status
                          case "Allocated":
                            statusClass = "success";
                            network_link_allocated_num += response.bandwidth;
                            break;
                          case "Failed":
                            statusClass = "danger";
                            break;
                          case "Pending":
                            statusClass = "warning";
                            break;
                          default:
                            statusClass = "";
                        }
                        network_link_total_num += response.bandwidth;
                        $("tr#" + rowId).removeClass("warning").addClass(statusClass);
                        $("tr#" + rowId + " .status").text(response.status);
                        if(response.status == "Failed") {
                          htmlStr = ' <span class="btn btn-danger btn-xs data-toggle="tooltip" data-placement="top" title="' + response.error  + '"><span class="glyphicon glyphicon-info-sign"></span></span>'; 
                          $("tr#" + rowId + " .status").append(htmlStr);
                        }
                        update_bandwidth_counters();
                      }
                  };    
                  xmlHttp.open("POST", "proxy.php", true);
                  xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                  xmlHttp.send("request=user_request_link&source=" + s + "&destination=" + d + "&bandwidth=" + b + "&cost=" + c);
            }
            
            function update_bandwidth_counters() {
              network_link_allocated.text(network_link_allocated_num); // Update allocated bandwidth number
              network_link_total.text(network_link_total_num);// Update total bandwidth number
            }
            
          function add_network_link_row(id, s, d, b) {
            htmlStr = "<tr id=\"" + id + "\" class=\"warning\">";
            htmlStr += "<td>" + s + "</td>";
            htmlStr += "<td>" + d + "</td>";
            htmlStr += "<td>" + b + "</td>";
            htmlStr += "<td class=\"status\">Pending</td>";
            network_link_list.prepend(htmlStr);
          }          
          
            
          function capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
          }
        </script>
        
<?php require_once("./includes/footer.php"); ?>
