<?php
    $tab = "network";
    $require_login = true;
    require_once("./includes/header.php");
?>
        <div id="diagram" class="container">
            <div class="row">
                <div class="col-md-5">
                    <h3 class="text-primary">Physical Network Topology</h3>
                </div>
                <div class="col-md-7 btn-list" style="margin-top: 20px;">
					<span id="request-link-config-btn" class="btn btn-default btn-sm">Export Physical Link Configuration <span class="glyphicon glyphicon-download-alt"></span></span>
                </div>
            </div>
            <!-- 
           	 <div class="network-links">
                Network Links:
                <div class="btn-group">
                  <button class="btn" onclick="networkLink(1)">1</button>
                  <button class="btn" onclick="networkLink(2)">2</button>
                  <button class="btn" onclick="networkLink(3)">3</button>
                  <button class="btn" onclick="networkLink(4)">4</button>
                  <button class="btn" onclick="networkLink(5)">5</button> 
                </div>
             </div> -->
             <div class="graph">
             </div>
        </div>

        <script>
			$(function() {
				// Listener for rlink export button
                $("#request-link-config-btn").on("click", function() {
                    get_physical_link_list();
                });		
			});				
				 				
			function get_physical_link_list() {
                  var xmlHttp = new XMLHttpRequest();
                  
                  xmlHttp.onload = function() {
                      if (xmlHttp.status == 200) {  
						  var response = xmlHttp.responseText;
                          response = JSON.parse(response);
					  
                          var xmlStr = "<link-config>\n";
                          var current = null;
						  
                          for (var i = 0; i<response.length; i++){ // Loop through physical links to build link xml elements
                            current = response[i];
                            xmlStr += "\t<link src=\"" + current.source + "\" dst=\"" + current.destination + "\" capacity=\"" + current.capacity + "\" cost=\"" + current.cost + "\"/>\n";
							xmlStr += "\t<link src=\"" + current.destination + "\" dst=\"" + current.source + "\" capacity=\"" + current.capacity + "\" cost=\"" + current.cost + "\"/>\n\n";
                          }						  
                          xmlStr += "</link-config>";
						  
						//export xmlStr as xml file  
						var element = document.createElement('a');
						element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(xmlStr));
						element.setAttribute('download', 'link-config.xml');
						element.style.display = 'none';
						document.body.appendChild(element);
						element.click();
						document.body.removeChild(element);
                      }
                  };    
                  xmlHttp.open("GET", "proxy.php?request=admin_physical_links", true);
                  xmlHttp.send();
            }			
		</script>

        

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        
    </body>
    <script src="http://d3js.org/d3.v3.min.js"></script>
 	<script src="js/app.js"></script>
    
<?php require_once("./includes/footer.php"); ?>