<?php
  require_once("./classes/NaaS.php");
  
  $tab = isset($tab) ? $tab : "";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>NaaS Project</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="css/app.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        
    </head>
    <body>
    
      <div id="controller-modal" class="modal fade">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h3 class="modal-title text-primary">Controller Settings</h3>
                  </div>
                  <div class="modal-body">
                      <form class="form-horizontal">
                          <div class="form-group">
                              <label class="col-sm-4 control-label">IP Address:</label>
                              <div class="col-sm-6">
                                <?php
                                  $cont_ip = $control->get_controller_ip();
                                ?>
                                <div class="radio">
                                  <label>
                                    <input type="radio" name="contollerIP" <?php echo $cont_ip == "127.0.0.1" ? "checked" : ""; ?> value="local"> <?php echo $control->get_client_ip(); ?> <small>(local)</small>
                                  </label>
                                </div>
                                <div class="radio">
                                  <label>
                                    <input type="radio" name="contollerIP" value="custom" id="controllerIPCustomRadio" <?php echo $cont_ip != "127.0.0.1" ? "checked" : ""; ?>> <input type="text" class="form-control" name="controllerIPCustom" id="controllerIPCustomInput" value="<?php echo $cont_ip != "127.0.0.1" ? $cont_ip : ""; ?>" />
                                  </label>
                                </div>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-4 control-label">Port:</label>
                              <div class="col-sm-3">
                                <input type="text" name="contollerPort" class="form-control" value="<?php echo $control->get_controller_port(); ?>" />
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-4 control-label">Location:</label>
                              <div class="col-sm-6">
                                <input type="text" name="contollerLocation" class="form-control" value="<?php echo $control->get_controller_location(); ?>" />
                              </div>
                          </div>
                      </form>
                  </div>
                  <div class="modal-footer">
                      <div class="btn-list">
                          <span id="cancel-link-request-btn" class="btn btn-danger pull-right" data-dismiss="modal"><span class="glyphicon glyphicon-remove-sign"></span> Cancel</span>
                          <span class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok-sign"></span> Save</span>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      <div id="nav-header" class="container">
          <div class="btn-list">
              <div class="btn-group pull-right" style="margin-left: 15px;">
                  <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <span class="glyphicon glyphicon-user"></span> <?php echo $_SESSION['username']; ?> <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                      <li><a href="./logout.php" class="btn-sm"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
                  </ul>
              </div>
              <span class="btn btn-default btn-sm" data-toggle="modal" data-target="#controller-modal"><span class="glyphicon glyphicon-hdd"></span> Controller</span>
          </div>
          <ul class="nav nav-tabs">
              <li role="presentation"<?php echo ($tab == "links") ?  " class=\"active\"" : ""; ?>><a href="./">Links</a></li>
              <li role="presentation"<?php echo ($tab == "network") ?  " class=\"active\"" : ""; ?>><a href="./network.php">Network</a></li>
          </ul>
      </div>