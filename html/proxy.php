<?php
  $require_login = true;
  require_once("./classes/NaaS.php");

  $request = array('type' => 'none');
  $output = "";
  $invalid_request = false;
  

  if(isset($_GET['request'])) { // GET request
    $request['type'] = 'GET';
    $request['name'] = $_GET['request'];
  }
  else if(isset($_POST)) { // POST request
    $request['type'] = 'POST';
    $request['name'] = $_POST['request'];
    $request['data']['source'] = $_POST['source'];
    $request['data']['destination'] = $_POST['destination'];
    $request['data']['bandwidth'] = doubleval($_POST['bandwidth']);
	$request['data']['cost'] = doubleval($_POST['cost']);
    $request['data']['status'] = "PENDING";
  }
  
  $request_whitelist = array( // Valid POST and GET request options
    'GET' => array( // Maps proxy request name to REST request name
      'user_virtual_links' => 'VL/',
	  'remove_all_vls' => 'Remove_all_VLs/',
      'admin_physical_links' => 'PL/'
    ),
    'POST' => array( // Maps proxy request name to REST request name
      'user_request_link' => 'VL/'
    )
  );
  
  if(isset($request_whitelist[$request['type']][$request['name']])) {
    $requestStr = $control->get_controller_url() . $request_whitelist[$request['type']][$request['name']] . $control->get_username();

    switch($request['type']) { // Decide type of request and further actions
      case 'GET': // GET request
          $ch = curl_init($requestStr); // Live cURL
          //$ch = curl_init("http://babbage.cs.missouri.edu/~mrrrk6/test/json_link_list_example.json"); // test on babbage
          $timeout = 5;
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
          $output = curl_exec($ch);
          curl_close($ch);
        break;
      
      case 'POST': // POST request
          $timeout = 5;
          $data_string = "[" . json_encode($request['data']) . "]";
          $ch = curl_init($requestStr); // Live cURL
          //$ch = curl_init("http://babbage.cs.missouri.edu/~mrrrk6/test/request_link_curl_response_example.php"); // test on babbage
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
          curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);  
          curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
              'Content-Type: application/json',                                                                                
              'Content-Length: ' . strlen($data_string))                                                                       
          );   
          $output = curl_exec($ch);
          curl_close($ch);
        break;
        
      default: // Not a GET or POST request
        $invalid_request = true;
    }
  }
  else {
    $invalid_request = true;
  }
  
  if($invalid_request) { // Error message
    $output = "{'error': 'Invalid Request' , 'type': \"" . $request['type'] . "\"}";    
  }
  
  // Print out json result
  header("Content-Type: application/json");
  echo $output;

?>