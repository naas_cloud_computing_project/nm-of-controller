<?php
  session_start();
  
  $require_login = isset($require_login) ? $require_login : false;
  if($require_login) {
    if(!isset($_SESSION["username"])) {
      header("Location: ./login.php");
    }
  }

  class NaaS {
    private $data = array();
    
    public function NaaS() {
      $this->get_flat_file();
    }
    
    // Read JSON flat file contents and put into array for accessing
    private function get_flat_file() {
      $file_name = "./includes/store.json";
      $file_string = file_get_contents($file_name);
      $this->set_data(json_decode($file_string, true));
    }

    // Set data array
    private function set_data($d) {
      $this->data = $d;
    }
    
    // Return controller IP setting
    public function get_controller_ip() {
      if($this->data['controller']['ip'] == "local") { // If local, return local IP address
        return $this->get_client_ip();
      }
      else { // Else return custom IP address
        return $this->data['controller']['ip'];
      }
    }
    
    // Return controller port setting 
    public function get_controller_port() {
      return $this->data['controller']['port'];
    }
    
    // Return controller location setting 
    public function get_controller_location() {
      return $this->data['controller']['location'];
    }
    
    public function get_controller_url() {
      $str = "http://";
      $str .= $this->get_controller_ip();
      $str .= ($this->get_controller_port() != "") ? ":" . $this->get_controller_port() : "";
      $str .= $this->get_controller_location();
      return $str;
    }
    
    // Return controller location setting 
    public function get_username() {
      return $this->data['username'];
    }
    
    // Function to get the client IP address
    public function get_client_ip() {
      $ipaddress = '';
      if (getenv('HTTP_CLIENT_IP'))
          $ipaddress = getenv('HTTP_CLIENT_IP');
      else if(getenv('HTTP_X_FORWARDED_FOR'))
          $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
      else if(getenv('HTTP_X_FORWARDED'))
          $ipaddress = getenv('HTTP_X_FORWARDED');
      else if(getenv('HTTP_FORWARDED_FOR'))
          $ipaddress = getenv('HTTP_FORWARDED_FOR');
      else if(getenv('HTTP_FORWARDED'))
         $ipaddress = getenv('HTTP_FORWARDED');
      else if(getenv('REMOTE_ADDR'))
          $ipaddress = getenv('REMOTE_ADDR');
      else
          $ipaddress = 'UNKNOWN';
      return $ipaddress;
    }
  }
  
  // Instance of new class to be used throughout program
  $control = new NaaS();
?>